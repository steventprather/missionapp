import { element, by, ElementFinder } from 'protractor';

export class UserMissionComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-user-mission div table .btn-danger'));
  title = element.all(by.css('jhi-user-mission div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class UserMissionUpdatePage {
  pageTitle = element(by.id('jhi-user-mission-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  userIdInput = element(by.id('field_userId'));
  missionIdInput = element(by.id('field_missionId'));

  userCitySelect = element(by.id('field_userCity'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setUserIdInput(userId: string): Promise<void> {
    await this.userIdInput.sendKeys(userId);
  }

  async getUserIdInput(): Promise<string> {
    return await this.userIdInput.getAttribute('value');
  }

  async setMissionIdInput(missionId: string): Promise<void> {
    await this.missionIdInput.sendKeys(missionId);
  }

  async getMissionIdInput(): Promise<string> {
    return await this.missionIdInput.getAttribute('value');
  }

  async userCitySelectLastOption(): Promise<void> {
    await this.userCitySelect.all(by.tagName('option')).last().click();
  }

  async userCitySelectOption(option: string): Promise<void> {
    await this.userCitySelect.sendKeys(option);
  }

  getUserCitySelect(): ElementFinder {
    return this.userCitySelect;
  }

  async getUserCitySelectedOption(): Promise<string> {
    return await this.userCitySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class UserMissionDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-userMission-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-userMission'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
