import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { UserMissionComponentsPage, UserMissionDeleteDialog, UserMissionUpdatePage } from './user-mission.page-object';

const expect = chai.expect;

describe('UserMission e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userMissionComponentsPage: UserMissionComponentsPage;
  let userMissionUpdatePage: UserMissionUpdatePage;
  let userMissionDeleteDialog: UserMissionDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserMissions', async () => {
    await navBarPage.goToEntity('user-mission');
    userMissionComponentsPage = new UserMissionComponentsPage();
    await browser.wait(ec.visibilityOf(userMissionComponentsPage.title), 5000);
    expect(await userMissionComponentsPage.getTitle()).to.eq('gatewayApp.responsesUserMission.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(userMissionComponentsPage.entities), ec.visibilityOf(userMissionComponentsPage.noResult)),
      1000
    );
  });

  it('should load create UserMission page', async () => {
    await userMissionComponentsPage.clickOnCreateButton();
    userMissionUpdatePage = new UserMissionUpdatePage();
    expect(await userMissionUpdatePage.getPageTitle()).to.eq('gatewayApp.responsesUserMission.home.createOrEditLabel');
    await userMissionUpdatePage.cancel();
  });

  it('should create and save UserMissions', async () => {
    const nbButtonsBeforeCreate = await userMissionComponentsPage.countDeleteButtons();

    await userMissionComponentsPage.clickOnCreateButton();

    await promise.all([
      userMissionUpdatePage.setUserIdInput('userId'),
      userMissionUpdatePage.setMissionIdInput('5'),
      userMissionUpdatePage.userCitySelectLastOption(),
    ]);

    expect(await userMissionUpdatePage.getUserIdInput()).to.eq('userId', 'Expected UserId value to be equals to userId');
    expect(await userMissionUpdatePage.getMissionIdInput()).to.eq('5', 'Expected missionId value to be equals to 5');

    await userMissionUpdatePage.save();
    expect(await userMissionUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userMissionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last UserMission', async () => {
    const nbButtonsBeforeDelete = await userMissionComponentsPage.countDeleteButtons();
    await userMissionComponentsPage.clickOnLastDeleteButton();

    userMissionDeleteDialog = new UserMissionDeleteDialog();
    expect(await userMissionDeleteDialog.getDialogTitle()).to.eq('gatewayApp.responsesUserMission.delete.question');
    await userMissionDeleteDialog.clickOnConfirmButton();

    expect(await userMissionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
