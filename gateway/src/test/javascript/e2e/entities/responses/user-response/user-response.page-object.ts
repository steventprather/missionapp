import { element, by, ElementFinder } from 'protractor';

export class UserResponseComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-user-response div table .btn-danger'));
  title = element.all(by.css('jhi-user-response div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class UserResponseUpdatePage {
  pageTitle = element(by.id('jhi-user-response-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  objectiveIdInput = element(by.id('field_objectiveId'));
  checkedInput = element(by.id('field_checked'));
  itemsInput = element(by.id('field_items'));
  pointsInput = element(by.id('field_points'));
  textInput = element(by.id('field_text'));

  userMissionSelect = element(by.id('field_userMission'));
  parentSelect = element(by.id('field_parent'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setObjectiveIdInput(objectiveId: string): Promise<void> {
    await this.objectiveIdInput.sendKeys(objectiveId);
  }

  async getObjectiveIdInput(): Promise<string> {
    return await this.objectiveIdInput.getAttribute('value');
  }

  getCheckedInput(): ElementFinder {
    return this.checkedInput;
  }

  async setItemsInput(items: string): Promise<void> {
    await this.itemsInput.sendKeys(items);
  }

  async getItemsInput(): Promise<string> {
    return await this.itemsInput.getAttribute('value');
  }

  async setPointsInput(points: string): Promise<void> {
    await this.pointsInput.sendKeys(points);
  }

  async getPointsInput(): Promise<string> {
    return await this.pointsInput.getAttribute('value');
  }

  async setTextInput(text: string): Promise<void> {
    await this.textInput.sendKeys(text);
  }

  async getTextInput(): Promise<string> {
    return await this.textInput.getAttribute('value');
  }

  async userMissionSelectLastOption(): Promise<void> {
    await this.userMissionSelect.all(by.tagName('option')).last().click();
  }

  async userMissionSelectOption(option: string): Promise<void> {
    await this.userMissionSelect.sendKeys(option);
  }

  getUserMissionSelect(): ElementFinder {
    return this.userMissionSelect;
  }

  async getUserMissionSelectedOption(): Promise<string> {
    return await this.userMissionSelect.element(by.css('option:checked')).getText();
  }

  async parentSelectLastOption(): Promise<void> {
    await this.parentSelect.all(by.tagName('option')).last().click();
  }

  async parentSelectOption(option: string): Promise<void> {
    await this.parentSelect.sendKeys(option);
  }

  getParentSelect(): ElementFinder {
    return this.parentSelect;
  }

  async getParentSelectedOption(): Promise<string> {
    return await this.parentSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class UserResponseDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-userResponse-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-userResponse'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
