import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { UserResponseComponentsPage, UserResponseDeleteDialog, UserResponseUpdatePage } from './user-response.page-object';

const expect = chai.expect;

describe('UserResponse e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userResponseComponentsPage: UserResponseComponentsPage;
  let userResponseUpdatePage: UserResponseUpdatePage;
  let userResponseDeleteDialog: UserResponseDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserResponses', async () => {
    await navBarPage.goToEntity('user-response');
    userResponseComponentsPage = new UserResponseComponentsPage();
    await browser.wait(ec.visibilityOf(userResponseComponentsPage.title), 5000);
    expect(await userResponseComponentsPage.getTitle()).to.eq('gatewayApp.responsesUserResponse.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(userResponseComponentsPage.entities), ec.visibilityOf(userResponseComponentsPage.noResult)),
      1000
    );
  });

  it('should load create UserResponse page', async () => {
    await userResponseComponentsPage.clickOnCreateButton();
    userResponseUpdatePage = new UserResponseUpdatePage();
    expect(await userResponseUpdatePage.getPageTitle()).to.eq('gatewayApp.responsesUserResponse.home.createOrEditLabel');
    await userResponseUpdatePage.cancel();
  });

  it('should create and save UserResponses', async () => {
    const nbButtonsBeforeCreate = await userResponseComponentsPage.countDeleteButtons();

    await userResponseComponentsPage.clickOnCreateButton();

    await promise.all([
      userResponseUpdatePage.setObjectiveIdInput('5'),
      userResponseUpdatePage.setItemsInput('5'),
      userResponseUpdatePage.setPointsInput('5'),
      userResponseUpdatePage.setTextInput('text'),
      userResponseUpdatePage.userMissionSelectLastOption(),
      userResponseUpdatePage.parentSelectLastOption(),
    ]);

    expect(await userResponseUpdatePage.getObjectiveIdInput()).to.eq('5', 'Expected objectiveId value to be equals to 5');
    const selectedChecked = userResponseUpdatePage.getCheckedInput();
    if (await selectedChecked.isSelected()) {
      await userResponseUpdatePage.getCheckedInput().click();
      expect(await userResponseUpdatePage.getCheckedInput().isSelected(), 'Expected checked not to be selected').to.be.false;
    } else {
      await userResponseUpdatePage.getCheckedInput().click();
      expect(await userResponseUpdatePage.getCheckedInput().isSelected(), 'Expected checked to be selected').to.be.true;
    }
    expect(await userResponseUpdatePage.getItemsInput()).to.eq('5', 'Expected items value to be equals to 5');
    expect(await userResponseUpdatePage.getPointsInput()).to.eq('5', 'Expected points value to be equals to 5');
    expect(await userResponseUpdatePage.getTextInput()).to.eq('text', 'Expected Text value to be equals to text');

    await userResponseUpdatePage.save();
    expect(await userResponseUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userResponseComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last UserResponse', async () => {
    const nbButtonsBeforeDelete = await userResponseComponentsPage.countDeleteButtons();
    await userResponseComponentsPage.clickOnLastDeleteButton();

    userResponseDeleteDialog = new UserResponseDeleteDialog();
    expect(await userResponseDeleteDialog.getDialogTitle()).to.eq('gatewayApp.responsesUserResponse.delete.question');
    await userResponseDeleteDialog.clickOnConfirmButton();

    expect(await userResponseComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
