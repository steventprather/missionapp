import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { UserCityComponentsPage, UserCityDeleteDialog, UserCityUpdatePage } from './user-city.page-object';

const expect = chai.expect;

describe('UserCity e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userCityComponentsPage: UserCityComponentsPage;
  let userCityUpdatePage: UserCityUpdatePage;
  let userCityDeleteDialog: UserCityDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserCities', async () => {
    await navBarPage.goToEntity('user-city');
    userCityComponentsPage = new UserCityComponentsPage();
    await browser.wait(ec.visibilityOf(userCityComponentsPage.title), 5000);
    expect(await userCityComponentsPage.getTitle()).to.eq('gatewayApp.responsesUserCity.home.title');
    await browser.wait(ec.or(ec.visibilityOf(userCityComponentsPage.entities), ec.visibilityOf(userCityComponentsPage.noResult)), 1000);
  });

  it('should load create UserCity page', async () => {
    await userCityComponentsPage.clickOnCreateButton();
    userCityUpdatePage = new UserCityUpdatePage();
    expect(await userCityUpdatePage.getPageTitle()).to.eq('gatewayApp.responsesUserCity.home.createOrEditLabel');
    await userCityUpdatePage.cancel();
  });

  it('should create and save UserCities', async () => {
    const nbButtonsBeforeCreate = await userCityComponentsPage.countDeleteButtons();

    await userCityComponentsPage.clickOnCreateButton();

    await promise.all([userCityUpdatePage.setUserIdInput('userId'), userCityUpdatePage.setCityIdInput('5')]);

    expect(await userCityUpdatePage.getUserIdInput()).to.eq('userId', 'Expected UserId value to be equals to userId');
    expect(await userCityUpdatePage.getCityIdInput()).to.eq('5', 'Expected cityId value to be equals to 5');

    await userCityUpdatePage.save();
    expect(await userCityUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userCityComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last UserCity', async () => {
    const nbButtonsBeforeDelete = await userCityComponentsPage.countDeleteButtons();
    await userCityComponentsPage.clickOnLastDeleteButton();

    userCityDeleteDialog = new UserCityDeleteDialog();
    expect(await userCityDeleteDialog.getDialogTitle()).to.eq('gatewayApp.responsesUserCity.delete.question');
    await userCityDeleteDialog.clickOnConfirmButton();

    expect(await userCityComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
