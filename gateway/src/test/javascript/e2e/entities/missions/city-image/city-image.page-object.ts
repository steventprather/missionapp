import { element, by, ElementFinder } from 'protractor';

export class CityImageComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-city-image div table .btn-danger'));
  title = element.all(by.css('jhi-city-image div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CityImageUpdatePage {
  pageTitle = element(by.id('jhi-city-image-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  fileNameInput = element(by.id('field_fileName'));

  citySelect = element(by.id('field_city'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setFileNameInput(fileName: string): Promise<void> {
    await this.fileNameInput.sendKeys(fileName);
  }

  async getFileNameInput(): Promise<string> {
    return await this.fileNameInput.getAttribute('value');
  }

  async citySelectLastOption(): Promise<void> {
    await this.citySelect.all(by.tagName('option')).last().click();
  }

  async citySelectOption(option: string): Promise<void> {
    await this.citySelect.sendKeys(option);
  }

  getCitySelect(): ElementFinder {
    return this.citySelect;
  }

  async getCitySelectedOption(): Promise<string> {
    return await this.citySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CityImageDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-cityImage-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-cityImage'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
