import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { CityImageComponentsPage, CityImageDeleteDialog, CityImageUpdatePage } from './city-image.page-object';

const expect = chai.expect;

describe('CityImage e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let cityImageComponentsPage: CityImageComponentsPage;
  let cityImageUpdatePage: CityImageUpdatePage;
  let cityImageDeleteDialog: CityImageDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CityImages', async () => {
    await navBarPage.goToEntity('city-image');
    cityImageComponentsPage = new CityImageComponentsPage();
    await browser.wait(ec.visibilityOf(cityImageComponentsPage.title), 5000);
    expect(await cityImageComponentsPage.getTitle()).to.eq('gatewayApp.missionsCityImage.home.title');
    await browser.wait(ec.or(ec.visibilityOf(cityImageComponentsPage.entities), ec.visibilityOf(cityImageComponentsPage.noResult)), 1000);
  });

  it('should load create CityImage page', async () => {
    await cityImageComponentsPage.clickOnCreateButton();
    cityImageUpdatePage = new CityImageUpdatePage();
    expect(await cityImageUpdatePage.getPageTitle()).to.eq('gatewayApp.missionsCityImage.home.createOrEditLabel');
    await cityImageUpdatePage.cancel();
  });

  it('should create and save CityImages', async () => {
    const nbButtonsBeforeCreate = await cityImageComponentsPage.countDeleteButtons();

    await cityImageComponentsPage.clickOnCreateButton();

    await promise.all([cityImageUpdatePage.setFileNameInput('fileName'), cityImageUpdatePage.citySelectLastOption()]);

    expect(await cityImageUpdatePage.getFileNameInput()).to.eq('fileName', 'Expected FileName value to be equals to fileName');

    await cityImageUpdatePage.save();
    expect(await cityImageUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await cityImageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last CityImage', async () => {
    const nbButtonsBeforeDelete = await cityImageComponentsPage.countDeleteButtons();
    await cityImageComponentsPage.clickOnLastDeleteButton();

    cityImageDeleteDialog = new CityImageDeleteDialog();
    expect(await cityImageDeleteDialog.getDialogTitle()).to.eq('gatewayApp.missionsCityImage.delete.question');
    await cityImageDeleteDialog.clickOnConfirmButton();

    expect(await cityImageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
