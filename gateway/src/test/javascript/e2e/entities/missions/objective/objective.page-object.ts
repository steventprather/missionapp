import { element, by, ElementFinder } from 'protractor';

export class ObjectiveComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-objective div table .btn-danger'));
  title = element.all(by.css('jhi-objective div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ObjectiveUpdatePage {
  pageTitle = element(by.id('jhi-objective-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nameInput = element(by.id('field_name'));
  textInput = element(by.id('field_text'));
  typeSelect = element(by.id('field_type'));
  orderInput = element(by.id('field_order'));
  maxPointsInput = element(by.id('field_maxPoints'));
  pointsPerItemInput = element(by.id('field_pointsPerItem'));
  pointsInput = element(by.id('field_points'));
  imageBaseInput = element(by.id('field_imageBase'));

  missionSelect = element(by.id('field_mission'));
  parentSelect = element(by.id('field_parent'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setTextInput(text: string): Promise<void> {
    await this.textInput.sendKeys(text);
  }

  async getTextInput(): Promise<string> {
    return await this.textInput.getAttribute('value');
  }

  async setTypeSelect(type: string): Promise<void> {
    await this.typeSelect.sendKeys(type);
  }

  async getTypeSelect(): Promise<string> {
    return await this.typeSelect.element(by.css('option:checked')).getText();
  }

  async typeSelectLastOption(): Promise<void> {
    await this.typeSelect.all(by.tagName('option')).last().click();
  }

  async setOrderInput(order: string): Promise<void> {
    await this.orderInput.sendKeys(order);
  }

  async getOrderInput(): Promise<string> {
    return await this.orderInput.getAttribute('value');
  }

  async setMaxPointsInput(maxPoints: string): Promise<void> {
    await this.maxPointsInput.sendKeys(maxPoints);
  }

  async getMaxPointsInput(): Promise<string> {
    return await this.maxPointsInput.getAttribute('value');
  }

  async setPointsPerItemInput(pointsPerItem: string): Promise<void> {
    await this.pointsPerItemInput.sendKeys(pointsPerItem);
  }

  async getPointsPerItemInput(): Promise<string> {
    return await this.pointsPerItemInput.getAttribute('value');
  }

  async setPointsInput(points: string): Promise<void> {
    await this.pointsInput.sendKeys(points);
  }

  async getPointsInput(): Promise<string> {
    return await this.pointsInput.getAttribute('value');
  }

  async setImageBaseInput(imageBase: string): Promise<void> {
    await this.imageBaseInput.sendKeys(imageBase);
  }

  async getImageBaseInput(): Promise<string> {
    return await this.imageBaseInput.getAttribute('value');
  }

  async missionSelectLastOption(): Promise<void> {
    await this.missionSelect.all(by.tagName('option')).last().click();
  }

  async missionSelectOption(option: string): Promise<void> {
    await this.missionSelect.sendKeys(option);
  }

  getMissionSelect(): ElementFinder {
    return this.missionSelect;
  }

  async getMissionSelectedOption(): Promise<string> {
    return await this.missionSelect.element(by.css('option:checked')).getText();
  }

  async parentSelectLastOption(): Promise<void> {
    await this.parentSelect.all(by.tagName('option')).last().click();
  }

  async parentSelectOption(option: string): Promise<void> {
    await this.parentSelect.sendKeys(option);
  }

  getParentSelect(): ElementFinder {
    return this.parentSelect;
  }

  async getParentSelectedOption(): Promise<string> {
    return await this.parentSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ObjectiveDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-objective-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-objective'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
