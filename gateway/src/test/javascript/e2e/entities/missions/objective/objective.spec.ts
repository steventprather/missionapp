import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { ObjectiveComponentsPage, ObjectiveDeleteDialog, ObjectiveUpdatePage } from './objective.page-object';

const expect = chai.expect;

describe('Objective e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let objectiveComponentsPage: ObjectiveComponentsPage;
  let objectiveUpdatePage: ObjectiveUpdatePage;
  let objectiveDeleteDialog: ObjectiveDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Objectives', async () => {
    await navBarPage.goToEntity('objective');
    objectiveComponentsPage = new ObjectiveComponentsPage();
    await browser.wait(ec.visibilityOf(objectiveComponentsPage.title), 5000);
    expect(await objectiveComponentsPage.getTitle()).to.eq('gatewayApp.missionsObjective.home.title');
    await browser.wait(ec.or(ec.visibilityOf(objectiveComponentsPage.entities), ec.visibilityOf(objectiveComponentsPage.noResult)), 1000);
  });

  it('should load create Objective page', async () => {
    await objectiveComponentsPage.clickOnCreateButton();
    objectiveUpdatePage = new ObjectiveUpdatePage();
    expect(await objectiveUpdatePage.getPageTitle()).to.eq('gatewayApp.missionsObjective.home.createOrEditLabel');
    await objectiveUpdatePage.cancel();
  });

  it('should create and save Objectives', async () => {
    const nbButtonsBeforeCreate = await objectiveComponentsPage.countDeleteButtons();

    await objectiveComponentsPage.clickOnCreateButton();

    await promise.all([
      objectiveUpdatePage.setNameInput('name'),
      objectiveUpdatePage.setTextInput('text'),
      objectiveUpdatePage.typeSelectLastOption(),
      objectiveUpdatePage.setOrderInput('5'),
      objectiveUpdatePage.setMaxPointsInput('5'),
      objectiveUpdatePage.setPointsPerItemInput('5'),
      objectiveUpdatePage.setPointsInput('5'),
      objectiveUpdatePage.setImageBaseInput('imageBase'),
      objectiveUpdatePage.missionSelectLastOption(),
      objectiveUpdatePage.parentSelectLastOption(),
    ]);

    expect(await objectiveUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await objectiveUpdatePage.getTextInput()).to.eq('text', 'Expected Text value to be equals to text');
    expect(await objectiveUpdatePage.getOrderInput()).to.eq('5', 'Expected order value to be equals to 5');
    expect(await objectiveUpdatePage.getMaxPointsInput()).to.eq('5', 'Expected maxPoints value to be equals to 5');
    expect(await objectiveUpdatePage.getPointsPerItemInput()).to.eq('5', 'Expected pointsPerItem value to be equals to 5');
    expect(await objectiveUpdatePage.getPointsInput()).to.eq('5', 'Expected points value to be equals to 5');
    expect(await objectiveUpdatePage.getImageBaseInput()).to.eq('imageBase', 'Expected ImageBase value to be equals to imageBase');

    await objectiveUpdatePage.save();
    expect(await objectiveUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await objectiveComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Objective', async () => {
    const nbButtonsBeforeDelete = await objectiveComponentsPage.countDeleteButtons();
    await objectiveComponentsPage.clickOnLastDeleteButton();

    objectiveDeleteDialog = new ObjectiveDeleteDialog();
    expect(await objectiveDeleteDialog.getDialogTitle()).to.eq('gatewayApp.missionsObjective.delete.question');
    await objectiveDeleteDialog.clickOnConfirmButton();

    expect(await objectiveComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
