import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { MissionImageComponentsPage, MissionImageDeleteDialog, MissionImageUpdatePage } from './mission-image.page-object';

const expect = chai.expect;

describe('MissionImage e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let missionImageComponentsPage: MissionImageComponentsPage;
  let missionImageUpdatePage: MissionImageUpdatePage;
  let missionImageDeleteDialog: MissionImageDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load MissionImages', async () => {
    await navBarPage.goToEntity('mission-image');
    missionImageComponentsPage = new MissionImageComponentsPage();
    await browser.wait(ec.visibilityOf(missionImageComponentsPage.title), 5000);
    expect(await missionImageComponentsPage.getTitle()).to.eq('gatewayApp.missionsMissionImage.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(missionImageComponentsPage.entities), ec.visibilityOf(missionImageComponentsPage.noResult)),
      1000
    );
  });

  it('should load create MissionImage page', async () => {
    await missionImageComponentsPage.clickOnCreateButton();
    missionImageUpdatePage = new MissionImageUpdatePage();
    expect(await missionImageUpdatePage.getPageTitle()).to.eq('gatewayApp.missionsMissionImage.home.createOrEditLabel');
    await missionImageUpdatePage.cancel();
  });

  it('should create and save MissionImages', async () => {
    const nbButtonsBeforeCreate = await missionImageComponentsPage.countDeleteButtons();

    await missionImageComponentsPage.clickOnCreateButton();

    await promise.all([missionImageUpdatePage.setFileNameInput('fileName'), missionImageUpdatePage.missionSelectLastOption()]);

    expect(await missionImageUpdatePage.getFileNameInput()).to.eq('fileName', 'Expected FileName value to be equals to fileName');

    await missionImageUpdatePage.save();
    expect(await missionImageUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await missionImageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last MissionImage', async () => {
    const nbButtonsBeforeDelete = await missionImageComponentsPage.countDeleteButtons();
    await missionImageComponentsPage.clickOnLastDeleteButton();

    missionImageDeleteDialog = new MissionImageDeleteDialog();
    expect(await missionImageDeleteDialog.getDialogTitle()).to.eq('gatewayApp.missionsMissionImage.delete.question');
    await missionImageDeleteDialog.clickOnConfirmButton();

    expect(await missionImageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
