import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { ObjectiveImageComponentsPage, ObjectiveImageDeleteDialog, ObjectiveImageUpdatePage } from './objective-image.page-object';

const expect = chai.expect;

describe('ObjectiveImage e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let objectiveImageComponentsPage: ObjectiveImageComponentsPage;
  let objectiveImageUpdatePage: ObjectiveImageUpdatePage;
  let objectiveImageDeleteDialog: ObjectiveImageDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ObjectiveImages', async () => {
    await navBarPage.goToEntity('objective-image');
    objectiveImageComponentsPage = new ObjectiveImageComponentsPage();
    await browser.wait(ec.visibilityOf(objectiveImageComponentsPage.title), 5000);
    expect(await objectiveImageComponentsPage.getTitle()).to.eq('gatewayApp.missionsObjectiveImage.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(objectiveImageComponentsPage.entities), ec.visibilityOf(objectiveImageComponentsPage.noResult)),
      1000
    );
  });

  it('should load create ObjectiveImage page', async () => {
    await objectiveImageComponentsPage.clickOnCreateButton();
    objectiveImageUpdatePage = new ObjectiveImageUpdatePage();
    expect(await objectiveImageUpdatePage.getPageTitle()).to.eq('gatewayApp.missionsObjectiveImage.home.createOrEditLabel');
    await objectiveImageUpdatePage.cancel();
  });

  it('should create and save ObjectiveImages', async () => {
    const nbButtonsBeforeCreate = await objectiveImageComponentsPage.countDeleteButtons();

    await objectiveImageComponentsPage.clickOnCreateButton();

    await promise.all([objectiveImageUpdatePage.setFileNameInput('fileName'), objectiveImageUpdatePage.objectiveSelectLastOption()]);

    expect(await objectiveImageUpdatePage.getFileNameInput()).to.eq('fileName', 'Expected FileName value to be equals to fileName');

    await objectiveImageUpdatePage.save();
    expect(await objectiveImageUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await objectiveImageComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last ObjectiveImage', async () => {
    const nbButtonsBeforeDelete = await objectiveImageComponentsPage.countDeleteButtons();
    await objectiveImageComponentsPage.clickOnLastDeleteButton();

    objectiveImageDeleteDialog = new ObjectiveImageDeleteDialog();
    expect(await objectiveImageDeleteDialog.getDialogTitle()).to.eq('gatewayApp.missionsObjectiveImage.delete.question');
    await objectiveImageDeleteDialog.clickOnConfirmButton();

    expect(await objectiveImageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
