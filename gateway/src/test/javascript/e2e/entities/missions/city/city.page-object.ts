import { element, by, ElementFinder } from 'protractor';

export class CityComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-city div table .btn-danger'));
  title = element.all(by.css('jhi-city div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CityUpdatePage {
  pageTitle = element(by.id('jhi-city-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nameInput = element(by.id('field_name'));
  textInput = element(by.id('field_text'));
  imageBaseInput = element(by.id('field_imageBase'));
  pointGoalInput = element(by.id('field_pointGoal'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setTextInput(text: string): Promise<void> {
    await this.textInput.sendKeys(text);
  }

  async getTextInput(): Promise<string> {
    return await this.textInput.getAttribute('value');
  }

  async setImageBaseInput(imageBase: string): Promise<void> {
    await this.imageBaseInput.sendKeys(imageBase);
  }

  async getImageBaseInput(): Promise<string> {
    return await this.imageBaseInput.getAttribute('value');
  }

  async setPointGoalInput(pointGoal: string): Promise<void> {
    await this.pointGoalInput.sendKeys(pointGoal);
  }

  async getPointGoalInput(): Promise<string> {
    return await this.pointGoalInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CityDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-city-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-city'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
