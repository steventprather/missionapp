import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { ObjectiveImageComponent } from 'app/entities/missions/objective-image/objective-image.component';
import { ObjectiveImageService } from 'app/entities/missions/objective-image/objective-image.service';
import { ObjectiveImage } from 'app/shared/model/missions/objective-image.model';

describe('Component Tests', () => {
  describe('ObjectiveImage Management Component', () => {
    let comp: ObjectiveImageComponent;
    let fixture: ComponentFixture<ObjectiveImageComponent>;
    let service: ObjectiveImageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [ObjectiveImageComponent],
      })
        .overrideTemplate(ObjectiveImageComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ObjectiveImageComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ObjectiveImageService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ObjectiveImage(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.objectiveImages && comp.objectiveImages[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
