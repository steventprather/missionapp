import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { CityImageUpdateComponent } from 'app/entities/missions/city-image/city-image-update.component';
import { CityImageService } from 'app/entities/missions/city-image/city-image.service';
import { CityImage } from 'app/shared/model/missions/city-image.model';

describe('Component Tests', () => {
  describe('CityImage Management Update Component', () => {
    let comp: CityImageUpdateComponent;
    let fixture: ComponentFixture<CityImageUpdateComponent>;
    let service: CityImageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [CityImageUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CityImageUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CityImageUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CityImageService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CityImage(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CityImage();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
