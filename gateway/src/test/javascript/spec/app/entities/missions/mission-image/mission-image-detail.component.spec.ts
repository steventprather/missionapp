import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { MissionImageDetailComponent } from 'app/entities/missions/mission-image/mission-image-detail.component';
import { MissionImage } from 'app/shared/model/missions/mission-image.model';

describe('Component Tests', () => {
  describe('MissionImage Management Detail Component', () => {
    let comp: MissionImageDetailComponent;
    let fixture: ComponentFixture<MissionImageDetailComponent>;
    const route = ({ data: of({ missionImage: new MissionImage(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [MissionImageDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(MissionImageDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MissionImageDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load missionImage on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.missionImage).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
