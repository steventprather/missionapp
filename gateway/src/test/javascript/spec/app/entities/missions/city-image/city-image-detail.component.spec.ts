import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { CityImageDetailComponent } from 'app/entities/missions/city-image/city-image-detail.component';
import { CityImage } from 'app/shared/model/missions/city-image.model';

describe('Component Tests', () => {
  describe('CityImage Management Detail Component', () => {
    let comp: CityImageDetailComponent;
    let fixture: ComponentFixture<CityImageDetailComponent>;
    const route = ({ data: of({ cityImage: new CityImage(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [CityImageDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CityImageDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CityImageDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load cityImage on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cityImage).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
