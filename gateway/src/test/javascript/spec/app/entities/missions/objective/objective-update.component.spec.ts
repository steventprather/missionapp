import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { ObjectiveUpdateComponent } from 'app/entities/missions/objective/objective-update.component';
import { ObjectiveService } from 'app/entities/missions/objective/objective.service';
import { Objective } from 'app/shared/model/missions/objective.model';

describe('Component Tests', () => {
  describe('Objective Management Update Component', () => {
    let comp: ObjectiveUpdateComponent;
    let fixture: ComponentFixture<ObjectiveUpdateComponent>;
    let service: ObjectiveService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [ObjectiveUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ObjectiveUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ObjectiveUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ObjectiveService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Objective(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Objective();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
