import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { ObjectiveImageDetailComponent } from 'app/entities/missions/objective-image/objective-image-detail.component';
import { ObjectiveImage } from 'app/shared/model/missions/objective-image.model';

describe('Component Tests', () => {
  describe('ObjectiveImage Management Detail Component', () => {
    let comp: ObjectiveImageDetailComponent;
    let fixture: ComponentFixture<ObjectiveImageDetailComponent>;
    const route = ({ data: of({ objectiveImage: new ObjectiveImage(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [ObjectiveImageDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ObjectiveImageDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ObjectiveImageDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load objectiveImage on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.objectiveImage).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
