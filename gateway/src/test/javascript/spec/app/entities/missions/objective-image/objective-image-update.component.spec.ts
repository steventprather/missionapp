import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { ObjectiveImageUpdateComponent } from 'app/entities/missions/objective-image/objective-image-update.component';
import { ObjectiveImageService } from 'app/entities/missions/objective-image/objective-image.service';
import { ObjectiveImage } from 'app/shared/model/missions/objective-image.model';

describe('Component Tests', () => {
  describe('ObjectiveImage Management Update Component', () => {
    let comp: ObjectiveImageUpdateComponent;
    let fixture: ComponentFixture<ObjectiveImageUpdateComponent>;
    let service: ObjectiveImageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [ObjectiveImageUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ObjectiveImageUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ObjectiveImageUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ObjectiveImageService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ObjectiveImage(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ObjectiveImage();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
