import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { ObjectiveDetailComponent } from 'app/entities/missions/objective/objective-detail.component';
import { Objective } from 'app/shared/model/missions/objective.model';

describe('Component Tests', () => {
  describe('Objective Management Detail Component', () => {
    let comp: ObjectiveDetailComponent;
    let fixture: ComponentFixture<ObjectiveDetailComponent>;
    const route = ({ data: of({ objective: new Objective(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [ObjectiveDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ObjectiveDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ObjectiveDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load objective on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.objective).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
