import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { CityImageComponent } from 'app/entities/missions/city-image/city-image.component';
import { CityImageService } from 'app/entities/missions/city-image/city-image.service';
import { CityImage } from 'app/shared/model/missions/city-image.model';

describe('Component Tests', () => {
  describe('CityImage Management Component', () => {
    let comp: CityImageComponent;
    let fixture: ComponentFixture<CityImageComponent>;
    let service: CityImageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [CityImageComponent],
      })
        .overrideTemplate(CityImageComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CityImageComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CityImageService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CityImage(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cityImages && comp.cityImages[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
