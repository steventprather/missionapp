import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { MissionImageUpdateComponent } from 'app/entities/missions/mission-image/mission-image-update.component';
import { MissionImageService } from 'app/entities/missions/mission-image/mission-image.service';
import { MissionImage } from 'app/shared/model/missions/mission-image.model';

describe('Component Tests', () => {
  describe('MissionImage Management Update Component', () => {
    let comp: MissionImageUpdateComponent;
    let fixture: ComponentFixture<MissionImageUpdateComponent>;
    let service: MissionImageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [MissionImageUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(MissionImageUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MissionImageUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MissionImageService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MissionImage(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MissionImage();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
