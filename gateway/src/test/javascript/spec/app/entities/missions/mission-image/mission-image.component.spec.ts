import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { MissionImageComponent } from 'app/entities/missions/mission-image/mission-image.component';
import { MissionImageService } from 'app/entities/missions/mission-image/mission-image.service';
import { MissionImage } from 'app/shared/model/missions/mission-image.model';

describe('Component Tests', () => {
  describe('MissionImage Management Component', () => {
    let comp: MissionImageComponent;
    let fixture: ComponentFixture<MissionImageComponent>;
    let service: MissionImageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [MissionImageComponent],
      })
        .overrideTemplate(MissionImageComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MissionImageComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MissionImageService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MissionImage(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.missionImages && comp.missionImages[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
