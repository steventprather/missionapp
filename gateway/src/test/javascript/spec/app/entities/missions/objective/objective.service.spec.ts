import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ObjectiveService } from 'app/entities/missions/objective/objective.service';
import { IObjective, Objective } from 'app/shared/model/missions/objective.model';
import { ObjectiveType } from 'app/shared/model/enumerations/objective-type.model';

describe('Service Tests', () => {
  describe('Objective Service', () => {
    let injector: TestBed;
    let service: ObjectiveService;
    let httpMock: HttpTestingController;
    let elemDefault: IObjective;
    let expectedResult: IObjective | IObjective[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ObjectiveService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Objective(0, 'AAAAAAA', 'AAAAAAA', ObjectiveType.PARENT, 0, 0, 0, 0, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Objective', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Objective()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Objective', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            text: 'BBBBBB',
            type: 'BBBBBB',
            order: 1,
            maxPoints: 1,
            pointsPerItem: 1,
            points: 1,
            imageBase: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Objective', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            text: 'BBBBBB',
            type: 'BBBBBB',
            order: 1,
            maxPoints: 1,
            pointsPerItem: 1,
            points: 1,
            imageBase: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Objective', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
