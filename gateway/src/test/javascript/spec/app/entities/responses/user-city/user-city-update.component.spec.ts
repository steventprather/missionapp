import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { UserCityUpdateComponent } from 'app/entities/responses/user-city/user-city-update.component';
import { UserCityService } from 'app/entities/responses/user-city/user-city.service';
import { UserCity } from 'app/shared/model/responses/user-city.model';

describe('Component Tests', () => {
  describe('UserCity Management Update Component', () => {
    let comp: UserCityUpdateComponent;
    let fixture: ComponentFixture<UserCityUpdateComponent>;
    let service: UserCityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [UserCityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(UserCityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserCityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserCityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserCity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserCity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
