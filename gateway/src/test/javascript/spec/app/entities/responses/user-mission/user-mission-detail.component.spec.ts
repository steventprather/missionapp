import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { UserMissionDetailComponent } from 'app/entities/responses/user-mission/user-mission-detail.component';
import { UserMission } from 'app/shared/model/responses/user-mission.model';

describe('Component Tests', () => {
  describe('UserMission Management Detail Component', () => {
    let comp: UserMissionDetailComponent;
    let fixture: ComponentFixture<UserMissionDetailComponent>;
    const route = ({ data: of({ userMission: new UserMission(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [UserMissionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(UserMissionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserMissionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load userMission on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userMission).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
