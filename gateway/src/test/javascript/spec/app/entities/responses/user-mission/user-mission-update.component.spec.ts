import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { UserMissionUpdateComponent } from 'app/entities/responses/user-mission/user-mission-update.component';
import { UserMissionService } from 'app/entities/responses/user-mission/user-mission.service';
import { UserMission } from 'app/shared/model/responses/user-mission.model';

describe('Component Tests', () => {
  describe('UserMission Management Update Component', () => {
    let comp: UserMissionUpdateComponent;
    let fixture: ComponentFixture<UserMissionUpdateComponent>;
    let service: UserMissionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [UserMissionUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(UserMissionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserMissionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserMissionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserMission(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserMission();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
