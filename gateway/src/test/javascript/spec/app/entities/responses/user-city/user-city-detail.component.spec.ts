import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { UserCityDetailComponent } from 'app/entities/responses/user-city/user-city-detail.component';
import { UserCity } from 'app/shared/model/responses/user-city.model';

describe('Component Tests', () => {
  describe('UserCity Management Detail Component', () => {
    let comp: UserCityDetailComponent;
    let fixture: ComponentFixture<UserCityDetailComponent>;
    const route = ({ data: of({ userCity: new UserCity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [UserCityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(UserCityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserCityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load userCity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userCity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
