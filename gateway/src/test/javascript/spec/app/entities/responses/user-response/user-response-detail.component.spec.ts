import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { UserResponseDetailComponent } from 'app/entities/responses/user-response/user-response-detail.component';
import { UserResponse } from 'app/shared/model/responses/user-response.model';

describe('Component Tests', () => {
  describe('UserResponse Management Detail Component', () => {
    let comp: UserResponseDetailComponent;
    let fixture: ComponentFixture<UserResponseDetailComponent>;
    const route = ({ data: of({ userResponse: new UserResponse(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayTestModule],
        declarations: [UserResponseDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(UserResponseDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserResponseDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load userResponse on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userResponse).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
