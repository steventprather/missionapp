import { ICityImage } from 'app/shared/model/missions/city-image.model';

export interface ICity {
  id?: number;
  name?: string;
  text?: string;
  imageBase?: string;
  pointGoal?: number;
  images?: ICityImage[];
}

export class City implements ICity {
  constructor(
    public id?: number,
    public name?: string,
    public text?: string,
    public imageBase?: string,
    public pointGoal?: number,
    public images?: ICityImage[]
  ) {}
}
