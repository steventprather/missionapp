export interface ICityImage {
  id?: number;
  fileName?: string;
  cityId?: number;
}

export class CityImage implements ICityImage {
  constructor(public id?: number, public fileName?: string, public cityId?: number) {}
}
