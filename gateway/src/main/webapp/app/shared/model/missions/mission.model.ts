import { IMissionImage } from 'app/shared/model/missions/mission-image.model';

export interface IMission {
  id?: number;
  name?: string;
  text?: string;
  imageBase?: string;
  images?: IMissionImage[];
  cityId?: number;
}

export class Mission implements IMission {
  constructor(
    public id?: number,
    public name?: string,
    public text?: string,
    public imageBase?: string,
    public images?: IMissionImage[],
    public cityId?: number
  ) {}
}
