import { IObjectiveImage } from 'app/shared/model/missions/objective-image.model';
import { ObjectiveType } from 'app/shared/model/enumerations/objective-type.model';

export interface IObjective {
  id?: number;
  name?: string;
  text?: string;
  type?: ObjectiveType;
  order?: number;
  maxPoints?: number;
  pointsPerItem?: number;
  points?: number;
  imageBase?: string;
  images?: IObjectiveImage[];
  missionId?: number;
  parentId?: number;
}

export class Objective implements IObjective {
  constructor(
    public id?: number,
    public name?: string,
    public text?: string,
    public type?: ObjectiveType,
    public order?: number,
    public maxPoints?: number,
    public pointsPerItem?: number,
    public points?: number,
    public imageBase?: string,
    public images?: IObjectiveImage[],
    public missionId?: number,
    public parentId?: number
  ) {}
}
