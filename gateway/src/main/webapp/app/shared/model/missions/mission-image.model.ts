export interface IMissionImage {
  id?: number;
  fileName?: string;
  missionId?: number;
}

export class MissionImage implements IMissionImage {
  constructor(public id?: number, public fileName?: string, public missionId?: number) {}
}
