export interface IObjectiveImage {
  id?: number;
  fileName?: string;
  objectiveId?: number;
}

export class ObjectiveImage implements IObjectiveImage {
  constructor(public id?: number, public fileName?: string, public objectiveId?: number) {}
}
