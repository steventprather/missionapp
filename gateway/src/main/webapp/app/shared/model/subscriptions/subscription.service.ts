import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ISubscriptionRequest } from './subscription-request.dto';
import { Observable } from 'rxjs';

type EntityResponseType = HttpResponse<ISubscriptionRequest>;

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  public resourceUrl = SERVER_API_URL + 'services/responses/api/subscription/';

  constructor(protected http: HttpClient) {}

  subscribe(subscription: ISubscriptionRequest): Observable<EntityResponseType> {
    return this.http.post<ISubscriptionRequest>(this.resourceUrl, subscription, { observe: 'response' });
  }
}
