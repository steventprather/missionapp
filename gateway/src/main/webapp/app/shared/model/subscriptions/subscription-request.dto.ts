export interface ISubscriptionRequest {
  target: SubscriptionTarget;
  id: number;
  subscribed: boolean;
}

export class SubscriptionRequest implements ISubscriptionRequest {
  constructor(public target: SubscriptionTarget, public id: number, public subscribed: boolean) {}
}

export enum SubscriptionTarget {
  City,
  Mission
}