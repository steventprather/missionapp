export interface IUserMission {
  id?: number;
  userId?: string;
  missionId?: number;
  userCityId?: number;
}

export class UserMission implements IUserMission {
  constructor(public id?: number, public userId?: string, public missionId?: number, public userCityId?: number) {}
}
