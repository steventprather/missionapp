export interface IUserResponse {
  id?: number;
  objectiveId?: number;
  checked?: boolean;
  items?: number;
  points?: number;
  text?: string;
  userMissionId?: number;
  parentId?: number;
}

export class UserResponse implements IUserResponse {
  constructor(
    public id?: number,
    public objectiveId?: number,
    public checked?: boolean,
    public items?: number,
    public points?: number,
    public text?: string,
    public userMissionId?: number,
    public parentId?: number
  ) {
    this.checked = this.checked || false;
  }
}
