export interface IUserCity {
  id?: number;
  userId?: string;
  cityId?: number;
}

export class UserCity implements IUserCity {
  constructor(public id?: number, public userId?: string, public cityId?: number) {}
}
