export const enum ObjectiveType {
  PARENT = 'PARENT',

  PERITEM = 'PERITEM',

  CHECKBOX = 'CHECKBOX',
}
