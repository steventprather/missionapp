import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserResponse, UserResponse } from 'app/shared/model/responses/user-response.model';
import { UserResponseService } from './user-response.service';
import { IUserMission } from 'app/shared/model/responses/user-mission.model';
import { UserMissionService } from 'app/entities/responses/user-mission/user-mission.service';

type SelectableEntity = IUserMission | IUserResponse;

@Component({
  selector: 'jhi-user-response-update',
  templateUrl: './user-response-update.component.html',
})
export class UserResponseUpdateComponent implements OnInit {
  isSaving = false;
  usermissions: IUserMission[] = [];
  userresponses: IUserResponse[] = [];

  editForm = this.fb.group({
    id: [],
    objectiveId: [],
    checked: [],
    items: [],
    points: [],
    text: [],
    userMissionId: [],
    parentId: [],
  });

  constructor(
    protected userResponseService: UserResponseService,
    protected userMissionService: UserMissionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userResponse }) => {
      this.updateForm(userResponse);

      this.userMissionService.query().subscribe((res: HttpResponse<IUserMission[]>) => (this.usermissions = res.body || []));

      this.userResponseService.query().subscribe((res: HttpResponse<IUserResponse[]>) => (this.userresponses = res.body || []));
    });
  }

  updateForm(userResponse: IUserResponse): void {
    this.editForm.patchValue({
      id: userResponse.id,
      objectiveId: userResponse.objectiveId,
      checked: userResponse.checked,
      items: userResponse.items,
      points: userResponse.points,
      text: userResponse.text,
      userMissionId: userResponse.userMissionId,
      parentId: userResponse.parentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userResponse = this.createFromForm();
    if (userResponse.id !== undefined) {
      this.subscribeToSaveResponse(this.userResponseService.update(userResponse));
    } else {
      this.subscribeToSaveResponse(this.userResponseService.create(userResponse));
    }
  }

  private createFromForm(): IUserResponse {
    return {
      ...new UserResponse(),
      id: this.editForm.get(['id'])!.value,
      objectiveId: this.editForm.get(['objectiveId'])!.value,
      checked: this.editForm.get(['checked'])!.value,
      items: this.editForm.get(['items'])!.value,
      points: this.editForm.get(['points'])!.value,
      text: this.editForm.get(['text'])!.value,
      userMissionId: this.editForm.get(['userMissionId'])!.value,
      parentId: this.editForm.get(['parentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserResponse>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
