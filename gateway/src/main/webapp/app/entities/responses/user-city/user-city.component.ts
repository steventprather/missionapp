import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUserCity } from 'app/shared/model/responses/user-city.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { UserCityService } from './user-city.service';
import { UserCityDeleteDialogComponent } from './user-city-delete-dialog.component';

@Component({
  selector: 'jhi-user-city',
  templateUrl: './user-city.component.html',
})
export class UserCityComponent implements OnInit, OnDestroy {
  userCities: IUserCity[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected userCityService: UserCityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.userCities = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.userCityService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IUserCity[]>) => this.paginateUserCities(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.userCities = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInUserCities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUserCity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUserCities(): void {
    this.eventSubscriber = this.eventManager.subscribe('userCityListModification', () => this.reset());
  }

  delete(userCity: IUserCity): void {
    const modalRef = this.modalService.open(UserCityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.userCity = userCity;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateUserCities(data: IUserCity[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.userCities.push(data[i]);
      }
    }
  }
}
