import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserCity } from 'app/shared/model/responses/user-city.model';

@Component({
  selector: 'jhi-user-city-detail',
  templateUrl: './user-city-detail.component.html',
})
export class UserCityDetailComponent implements OnInit {
  userCity: IUserCity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userCity }) => (this.userCity = userCity));
  }

  previousState(): void {
    window.history.back();
  }
}
