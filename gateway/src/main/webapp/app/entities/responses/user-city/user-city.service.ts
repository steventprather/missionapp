import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserCity } from 'app/shared/model/responses/user-city.model';

type EntityResponseType = HttpResponse<IUserCity>;
type EntityArrayResponseType = HttpResponse<IUserCity[]>;

@Injectable({ providedIn: 'root' })
export class UserCityService {
  public resourceUrl = SERVER_API_URL + 'services/responses/api/user-cities';

  constructor(protected http: HttpClient) {}

  create(userCity: IUserCity): Observable<EntityResponseType> {
    return this.http.post<IUserCity>(this.resourceUrl, userCity, { observe: 'response' });
  }

  update(userCity: IUserCity): Observable<EntityResponseType> {
    return this.http.put<IUserCity>(this.resourceUrl, userCity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserCity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserCity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
