import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserCity, UserCity } from 'app/shared/model/responses/user-city.model';
import { UserCityService } from './user-city.service';

@Component({
  selector: 'jhi-user-city-update',
  templateUrl: './user-city-update.component.html',
})
export class UserCityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userId: [],
    cityId: [],
  });

  constructor(protected userCityService: UserCityService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userCity }) => {
      this.updateForm(userCity);
    });
  }

  updateForm(userCity: IUserCity): void {
    this.editForm.patchValue({
      id: userCity.id,
      userId: userCity.userId,
      cityId: userCity.cityId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userCity = this.createFromForm();
    if (userCity.id !== undefined) {
      this.subscribeToSaveResponse(this.userCityService.update(userCity));
    } else {
      this.subscribeToSaveResponse(this.userCityService.create(userCity));
    }
  }

  private createFromForm(): IUserCity {
    return {
      ...new UserCity(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      cityId: this.editForm.get(['cityId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserCity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
