import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared/shared.module';
import { UserCityComponent } from './user-city.component';
import { UserCityDetailComponent } from './user-city-detail.component';
import { UserCityUpdateComponent } from './user-city-update.component';
import { UserCityDeleteDialogComponent } from './user-city-delete-dialog.component';
import { userCityRoute } from './user-city.route';

@NgModule({
  imports: [GatewaySharedModule, RouterModule.forChild(userCityRoute)],
  declarations: [UserCityComponent, UserCityDetailComponent, UserCityUpdateComponent, UserCityDeleteDialogComponent],
  entryComponents: [UserCityDeleteDialogComponent],
})
export class ResponsesUserCityModule {}
