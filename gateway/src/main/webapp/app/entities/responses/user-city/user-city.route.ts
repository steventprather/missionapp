import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUserCity, UserCity } from 'app/shared/model/responses/user-city.model';
import { UserCityService } from './user-city.service';
import { UserCityComponent } from './user-city.component';
import { UserCityDetailComponent } from './user-city-detail.component';
import { UserCityUpdateComponent } from './user-city-update.component';

@Injectable({ providedIn: 'root' })
export class UserCityResolve implements Resolve<IUserCity> {
  constructor(private service: UserCityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserCity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((userCity: HttpResponse<UserCity>) => {
          if (userCity.body) {
            return of(userCity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UserCity());
  }
}

export const userCityRoute: Routes = [
  {
    path: '',
    component: UserCityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.responsesUserCity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: UserCityDetailComponent,
    resolve: {
      userCity: UserCityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.responsesUserCity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: UserCityUpdateComponent,
    resolve: {
      userCity: UserCityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.responsesUserCity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: UserCityUpdateComponent,
    resolve: {
      userCity: UserCityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.responsesUserCity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
