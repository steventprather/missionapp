import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserCity } from 'app/shared/model/responses/user-city.model';
import { UserCityService } from './user-city.service';

@Component({
  templateUrl: './user-city-delete-dialog.component.html',
})
export class UserCityDeleteDialogComponent {
  userCity?: IUserCity;

  constructor(protected userCityService: UserCityService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.userCityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('userCityListModification');
      this.activeModal.close();
    });
  }
}
