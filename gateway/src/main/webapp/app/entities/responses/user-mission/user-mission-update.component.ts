import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserMission, UserMission } from 'app/shared/model/responses/user-mission.model';
import { UserMissionService } from './user-mission.service';
import { IUserCity } from 'app/shared/model/responses/user-city.model';
import { UserCityService } from 'app/entities/responses/user-city/user-city.service';

@Component({
  selector: 'jhi-user-mission-update',
  templateUrl: './user-mission-update.component.html',
})
export class UserMissionUpdateComponent implements OnInit {
  isSaving = false;
  usercities: IUserCity[] = [];

  editForm = this.fb.group({
    id: [],
    userId: [],
    missionId: [],
    userCityId: [],
  });

  constructor(
    protected userMissionService: UserMissionService,
    protected userCityService: UserCityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userMission }) => {
      this.updateForm(userMission);

      this.userCityService.query().subscribe((res: HttpResponse<IUserCity[]>) => (this.usercities = res.body || []));
    });
  }

  updateForm(userMission: IUserMission): void {
    this.editForm.patchValue({
      id: userMission.id,
      userId: userMission.userId,
      missionId: userMission.missionId,
      userCityId: userMission.userCityId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userMission = this.createFromForm();
    if (userMission.id !== undefined) {
      this.subscribeToSaveResponse(this.userMissionService.update(userMission));
    } else {
      this.subscribeToSaveResponse(this.userMissionService.create(userMission));
    }
  }

  private createFromForm(): IUserMission {
    return {
      ...new UserMission(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      missionId: this.editForm.get(['missionId'])!.value,
      userCityId: this.editForm.get(['userCityId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserMission>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUserCity): any {
    return item.id;
  }
}
