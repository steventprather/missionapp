import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared/shared.module';
import { UserMissionComponent } from './user-mission.component';
import { UserMissionDetailComponent } from './user-mission-detail.component';
import { UserMissionUpdateComponent } from './user-mission-update.component';
import { UserMissionDeleteDialogComponent } from './user-mission-delete-dialog.component';
import { userMissionRoute } from './user-mission.route';

@NgModule({
  imports: [GatewaySharedModule, RouterModule.forChild(userMissionRoute)],
  declarations: [UserMissionComponent, UserMissionDetailComponent, UserMissionUpdateComponent, UserMissionDeleteDialogComponent],
  entryComponents: [UserMissionDeleteDialogComponent],
})
export class ResponsesUserMissionModule {}
