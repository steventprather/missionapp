import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserMission } from 'app/shared/model/responses/user-mission.model';

@Component({
  selector: 'jhi-user-mission-detail',
  templateUrl: './user-mission-detail.component.html',
})
export class UserMissionDetailComponent implements OnInit {
  userMission: IUserMission | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userMission }) => (this.userMission = userMission));
  }

  previousState(): void {
    window.history.back();
  }
}
