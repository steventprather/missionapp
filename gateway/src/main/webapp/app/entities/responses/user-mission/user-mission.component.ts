import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUserMission } from 'app/shared/model/responses/user-mission.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { UserMissionService } from './user-mission.service';
import { UserMissionDeleteDialogComponent } from './user-mission-delete-dialog.component';

@Component({
  selector: 'jhi-user-mission',
  templateUrl: './user-mission.component.html',
})
export class UserMissionComponent implements OnInit, OnDestroy {
  userMissions: IUserMission[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected userMissionService: UserMissionService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.userMissions = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.userMissionService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IUserMission[]>) => this.paginateUserMissions(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.userMissions = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInUserMissions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUserMission): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUserMissions(): void {
    this.eventSubscriber = this.eventManager.subscribe('userMissionListModification', () => this.reset());
  }

  delete(userMission: IUserMission): void {
    const modalRef = this.modalService.open(UserMissionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.userMission = userMission;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateUserMissions(data: IUserMission[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.userMissions.push(data[i]);
      }
    }
  }
}
