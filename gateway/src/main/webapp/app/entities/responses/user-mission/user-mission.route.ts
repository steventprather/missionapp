import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUserMission, UserMission } from 'app/shared/model/responses/user-mission.model';
import { UserMissionService } from './user-mission.service';
import { UserMissionComponent } from './user-mission.component';
import { UserMissionDetailComponent } from './user-mission-detail.component';
import { UserMissionUpdateComponent } from './user-mission-update.component';

@Injectable({ providedIn: 'root' })
export class UserMissionResolve implements Resolve<IUserMission> {
  constructor(private service: UserMissionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserMission> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((userMission: HttpResponse<UserMission>) => {
          if (userMission.body) {
            return of(userMission.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UserMission());
  }
}

export const userMissionRoute: Routes = [
  {
    path: '',
    component: UserMissionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.responsesUserMission.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: UserMissionDetailComponent,
    resolve: {
      userMission: UserMissionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.responsesUserMission.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: UserMissionUpdateComponent,
    resolve: {
      userMission: UserMissionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.responsesUserMission.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: UserMissionUpdateComponent,
    resolve: {
      userMission: UserMissionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.responsesUserMission.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
