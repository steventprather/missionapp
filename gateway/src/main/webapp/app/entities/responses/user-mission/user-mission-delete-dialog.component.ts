import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserMission } from 'app/shared/model/responses/user-mission.model';
import { UserMissionService } from './user-mission.service';

@Component({
  templateUrl: './user-mission-delete-dialog.component.html',
})
export class UserMissionDeleteDialogComponent {
  userMission?: IUserMission;

  constructor(
    protected userMissionService: UserMissionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.userMissionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('userMissionListModification');
      this.activeModal.close();
    });
  }
}
