import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IObjectiveImage } from 'app/shared/model/missions/objective-image.model';

type EntityResponseType = HttpResponse<IObjectiveImage>;
type EntityArrayResponseType = HttpResponse<IObjectiveImage[]>;

@Injectable({ providedIn: 'root' })
export class ObjectiveImageService {
  public resourceUrl = SERVER_API_URL + 'services/missions/api/objective-images';

  constructor(protected http: HttpClient) {}

  create(objectiveImage: IObjectiveImage): Observable<EntityResponseType> {
    return this.http.post<IObjectiveImage>(this.resourceUrl, objectiveImage, { observe: 'response' });
  }

  update(objectiveImage: IObjectiveImage): Observable<EntityResponseType> {
    return this.http.put<IObjectiveImage>(this.resourceUrl, objectiveImage, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IObjectiveImage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IObjectiveImage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
