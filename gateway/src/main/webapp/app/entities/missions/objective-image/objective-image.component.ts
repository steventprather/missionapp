import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IObjectiveImage } from 'app/shared/model/missions/objective-image.model';
import { ObjectiveImageService } from './objective-image.service';
import { ObjectiveImageDeleteDialogComponent } from './objective-image-delete-dialog.component';

@Component({
  selector: 'jhi-objective-image',
  templateUrl: './objective-image.component.html',
})
export class ObjectiveImageComponent implements OnInit, OnDestroy {
  objectiveImages?: IObjectiveImage[];
  eventSubscriber?: Subscription;

  constructor(
    protected objectiveImageService: ObjectiveImageService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.objectiveImageService.query().subscribe((res: HttpResponse<IObjectiveImage[]>) => (this.objectiveImages = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInObjectiveImages();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IObjectiveImage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInObjectiveImages(): void {
    this.eventSubscriber = this.eventManager.subscribe('objectiveImageListModification', () => this.loadAll());
  }

  delete(objectiveImage: IObjectiveImage): void {
    const modalRef = this.modalService.open(ObjectiveImageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.objectiveImage = objectiveImage;
  }
}
