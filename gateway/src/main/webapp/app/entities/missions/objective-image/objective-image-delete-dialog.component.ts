import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IObjectiveImage } from 'app/shared/model/missions/objective-image.model';
import { ObjectiveImageService } from './objective-image.service';

@Component({
  templateUrl: './objective-image-delete-dialog.component.html',
})
export class ObjectiveImageDeleteDialogComponent {
  objectiveImage?: IObjectiveImage;

  constructor(
    protected objectiveImageService: ObjectiveImageService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.objectiveImageService.delete(id).subscribe(() => {
      this.eventManager.broadcast('objectiveImageListModification');
      this.activeModal.close();
    });
  }
}
