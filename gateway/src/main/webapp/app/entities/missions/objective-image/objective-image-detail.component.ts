import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IObjectiveImage } from 'app/shared/model/missions/objective-image.model';

@Component({
  selector: 'jhi-objective-image-detail',
  templateUrl: './objective-image-detail.component.html',
})
export class ObjectiveImageDetailComponent implements OnInit {
  objectiveImage: IObjectiveImage | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ objectiveImage }) => (this.objectiveImage = objectiveImage));
  }

  previousState(): void {
    window.history.back();
  }
}
