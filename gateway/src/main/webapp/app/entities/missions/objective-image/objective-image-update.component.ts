import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IObjectiveImage, ObjectiveImage } from 'app/shared/model/missions/objective-image.model';
import { ObjectiveImageService } from './objective-image.service';
import { IObjective } from 'app/shared/model/missions/objective.model';
import { ObjectiveService } from 'app/entities/missions/objective/objective.service';

@Component({
  selector: 'jhi-objective-image-update',
  templateUrl: './objective-image-update.component.html',
})
export class ObjectiveImageUpdateComponent implements OnInit {
  isSaving = false;
  objectives: IObjective[] = [];

  editForm = this.fb.group({
    id: [],
    fileName: [],
    objectiveId: [],
  });

  constructor(
    protected objectiveImageService: ObjectiveImageService,
    protected objectiveService: ObjectiveService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ objectiveImage }) => {
      this.updateForm(objectiveImage);

      this.objectiveService.query().subscribe((res: HttpResponse<IObjective[]>) => (this.objectives = res.body || []));
    });
  }

  updateForm(objectiveImage: IObjectiveImage): void {
    this.editForm.patchValue({
      id: objectiveImage.id,
      fileName: objectiveImage.fileName,
      objectiveId: objectiveImage.objectiveId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const objectiveImage = this.createFromForm();
    if (objectiveImage.id !== undefined) {
      this.subscribeToSaveResponse(this.objectiveImageService.update(objectiveImage));
    } else {
      this.subscribeToSaveResponse(this.objectiveImageService.create(objectiveImage));
    }
  }

  private createFromForm(): IObjectiveImage {
    return {
      ...new ObjectiveImage(),
      id: this.editForm.get(['id'])!.value,
      fileName: this.editForm.get(['fileName'])!.value,
      objectiveId: this.editForm.get(['objectiveId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IObjectiveImage>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IObjective): any {
    return item.id;
  }
}
