import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IObjectiveImage, ObjectiveImage } from 'app/shared/model/missions/objective-image.model';
import { ObjectiveImageService } from './objective-image.service';
import { ObjectiveImageComponent } from './objective-image.component';
import { ObjectiveImageDetailComponent } from './objective-image-detail.component';
import { ObjectiveImageUpdateComponent } from './objective-image-update.component';

@Injectable({ providedIn: 'root' })
export class ObjectiveImageResolve implements Resolve<IObjectiveImage> {
  constructor(private service: ObjectiveImageService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IObjectiveImage> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((objectiveImage: HttpResponse<ObjectiveImage>) => {
          if (objectiveImage.body) {
            return of(objectiveImage.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ObjectiveImage());
  }
}

export const objectiveImageRoute: Routes = [
  {
    path: '',
    component: ObjectiveImageComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsObjectiveImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ObjectiveImageDetailComponent,
    resolve: {
      objectiveImage: ObjectiveImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsObjectiveImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ObjectiveImageUpdateComponent,
    resolve: {
      objectiveImage: ObjectiveImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsObjectiveImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ObjectiveImageUpdateComponent,
    resolve: {
      objectiveImage: ObjectiveImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsObjectiveImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
