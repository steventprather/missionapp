import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared/shared.module';
import { ObjectiveImageComponent } from './objective-image.component';
import { ObjectiveImageDetailComponent } from './objective-image-detail.component';
import { ObjectiveImageUpdateComponent } from './objective-image-update.component';
import { ObjectiveImageDeleteDialogComponent } from './objective-image-delete-dialog.component';
import { objectiveImageRoute } from './objective-image.route';

@NgModule({
  imports: [GatewaySharedModule, RouterModule.forChild(objectiveImageRoute)],
  declarations: [
    ObjectiveImageComponent,
    ObjectiveImageDetailComponent,
    ObjectiveImageUpdateComponent,
    ObjectiveImageDeleteDialogComponent,
  ],
  entryComponents: [ObjectiveImageDeleteDialogComponent],
})
export class MissionsObjectiveImageModule {}
