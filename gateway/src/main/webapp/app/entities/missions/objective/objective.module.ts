import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared/shared.module';
import { ObjectiveComponent } from './objective.component';
import { ObjectiveDetailComponent } from './objective-detail.component';
import { ObjectiveUpdateComponent } from './objective-update.component';
import { ObjectiveDeleteDialogComponent } from './objective-delete-dialog.component';
import { objectiveRoute } from './objective.route';

@NgModule({
  imports: [GatewaySharedModule, RouterModule.forChild(objectiveRoute)],
  declarations: [ObjectiveComponent, ObjectiveDetailComponent, ObjectiveUpdateComponent, ObjectiveDeleteDialogComponent],
  entryComponents: [ObjectiveDeleteDialogComponent],
})
export class MissionsObjectiveModule {}
