import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IObjective } from 'app/shared/model/missions/objective.model';

type EntityResponseType = HttpResponse<IObjective>;
type EntityArrayResponseType = HttpResponse<IObjective[]>;

@Injectable({ providedIn: 'root' })
export class ObjectiveService {
  public resourceUrl = SERVER_API_URL + 'services/missions/api/objectives';

  constructor(protected http: HttpClient) {}

  create(objective: IObjective): Observable<EntityResponseType> {
    return this.http.post<IObjective>(this.resourceUrl, objective, { observe: 'response' });
  }

  update(objective: IObjective): Observable<EntityResponseType> {
    return this.http.put<IObjective>(this.resourceUrl, objective, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IObjective>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IObjective[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
