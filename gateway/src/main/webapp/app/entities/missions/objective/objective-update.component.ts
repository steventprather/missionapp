import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IObjective, Objective } from 'app/shared/model/missions/objective.model';
import { ObjectiveService } from './objective.service';
import { IMission } from 'app/shared/model/missions/mission.model';
import { MissionService } from 'app/entities/missions/mission/mission.service';

type SelectableEntity = IMission | IObjective;

@Component({
  selector: 'jhi-objective-update',
  templateUrl: './objective-update.component.html',
})
export class ObjectiveUpdateComponent implements OnInit {
  isSaving = false;
  missions: IMission[] = [];
  objectives: IObjective[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    text: [],
    type: [],
    order: [],
    maxPoints: [],
    pointsPerItem: [],
    points: [],
    imageBase: [],
    missionId: [],
    parentId: [],
  });

  constructor(
    protected objectiveService: ObjectiveService,
    protected missionService: MissionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ objective }) => {
      this.updateForm(objective);

      this.missionService.query().subscribe((res: HttpResponse<IMission[]>) => (this.missions = res.body || []));

      this.objectiveService.query().subscribe((res: HttpResponse<IObjective[]>) => (this.objectives = res.body || []));
    });
  }

  updateForm(objective: IObjective): void {
    this.editForm.patchValue({
      id: objective.id,
      name: objective.name,
      text: objective.text,
      type: objective.type,
      order: objective.order,
      maxPoints: objective.maxPoints,
      pointsPerItem: objective.pointsPerItem,
      points: objective.points,
      imageBase: objective.imageBase,
      missionId: objective.missionId,
      parentId: objective.parentId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const objective = this.createFromForm();
    if (objective.id !== undefined) {
      this.subscribeToSaveResponse(this.objectiveService.update(objective));
    } else {
      this.subscribeToSaveResponse(this.objectiveService.create(objective));
    }
  }

  private createFromForm(): IObjective {
    return {
      ...new Objective(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      text: this.editForm.get(['text'])!.value,
      type: this.editForm.get(['type'])!.value,
      order: this.editForm.get(['order'])!.value,
      maxPoints: this.editForm.get(['maxPoints'])!.value,
      pointsPerItem: this.editForm.get(['pointsPerItem'])!.value,
      points: this.editForm.get(['points'])!.value,
      imageBase: this.editForm.get(['imageBase'])!.value,
      missionId: this.editForm.get(['missionId'])!.value,
      parentId: this.editForm.get(['parentId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IObjective>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
