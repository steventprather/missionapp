import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IObjective, Objective } from 'app/shared/model/missions/objective.model';
import { ObjectiveService } from './objective.service';
import { ObjectiveComponent } from './objective.component';
import { ObjectiveDetailComponent } from './objective-detail.component';
import { ObjectiveUpdateComponent } from './objective-update.component';

@Injectable({ providedIn: 'root' })
export class ObjectiveResolve implements Resolve<IObjective> {
  constructor(private service: ObjectiveService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IObjective> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((objective: HttpResponse<Objective>) => {
          if (objective.body) {
            return of(objective.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Objective());
  }
}

export const objectiveRoute: Routes = [
  {
    path: '',
    component: ObjectiveComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsObjective.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ObjectiveDetailComponent,
    resolve: {
      objective: ObjectiveResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsObjective.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ObjectiveUpdateComponent,
    resolve: {
      objective: ObjectiveResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsObjective.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ObjectiveUpdateComponent,
    resolve: {
      objective: ObjectiveResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsObjective.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
