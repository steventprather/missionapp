import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IObjective } from 'app/shared/model/missions/objective.model';
import { ObjectiveService } from './objective.service';

@Component({
  templateUrl: './objective-delete-dialog.component.html',
})
export class ObjectiveDeleteDialogComponent {
  objective?: IObjective;

  constructor(protected objectiveService: ObjectiveService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.objectiveService.delete(id).subscribe(() => {
      this.eventManager.broadcast('objectiveListModification');
      this.activeModal.close();
    });
  }
}
