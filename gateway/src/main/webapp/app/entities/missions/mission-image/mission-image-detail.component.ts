import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMissionImage } from 'app/shared/model/missions/mission-image.model';

@Component({
  selector: 'jhi-mission-image-detail',
  templateUrl: './mission-image-detail.component.html',
})
export class MissionImageDetailComponent implements OnInit {
  missionImage: IMissionImage | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ missionImage }) => (this.missionImage = missionImage));
  }

  previousState(): void {
    window.history.back();
  }
}
