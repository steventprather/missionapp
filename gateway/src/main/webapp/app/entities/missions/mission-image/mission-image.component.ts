import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMissionImage } from 'app/shared/model/missions/mission-image.model';
import { MissionImageService } from './mission-image.service';
import { MissionImageDeleteDialogComponent } from './mission-image-delete-dialog.component';

@Component({
  selector: 'jhi-mission-image',
  templateUrl: './mission-image.component.html',
})
export class MissionImageComponent implements OnInit, OnDestroy {
  missionImages?: IMissionImage[];
  eventSubscriber?: Subscription;

  constructor(
    protected missionImageService: MissionImageService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.missionImageService.query().subscribe((res: HttpResponse<IMissionImage[]>) => (this.missionImages = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMissionImages();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMissionImage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMissionImages(): void {
    this.eventSubscriber = this.eventManager.subscribe('missionImageListModification', () => this.loadAll());
  }

  delete(missionImage: IMissionImage): void {
    const modalRef = this.modalService.open(MissionImageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.missionImage = missionImage;
  }
}
