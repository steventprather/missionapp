import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMissionImage, MissionImage } from 'app/shared/model/missions/mission-image.model';
import { MissionImageService } from './mission-image.service';
import { MissionImageComponent } from './mission-image.component';
import { MissionImageDetailComponent } from './mission-image-detail.component';
import { MissionImageUpdateComponent } from './mission-image-update.component';

@Injectable({ providedIn: 'root' })
export class MissionImageResolve implements Resolve<IMissionImage> {
  constructor(private service: MissionImageService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMissionImage> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((missionImage: HttpResponse<MissionImage>) => {
          if (missionImage.body) {
            return of(missionImage.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MissionImage());
  }
}

export const missionImageRoute: Routes = [
  {
    path: '',
    component: MissionImageComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsMissionImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MissionImageDetailComponent,
    resolve: {
      missionImage: MissionImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsMissionImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MissionImageUpdateComponent,
    resolve: {
      missionImage: MissionImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsMissionImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MissionImageUpdateComponent,
    resolve: {
      missionImage: MissionImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsMissionImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
