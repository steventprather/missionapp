import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMissionImage, MissionImage } from 'app/shared/model/missions/mission-image.model';
import { MissionImageService } from './mission-image.service';
import { IMission } from 'app/shared/model/missions/mission.model';
import { MissionService } from 'app/entities/missions/mission/mission.service';

@Component({
  selector: 'jhi-mission-image-update',
  templateUrl: './mission-image-update.component.html',
})
export class MissionImageUpdateComponent implements OnInit {
  isSaving = false;
  missions: IMission[] = [];

  editForm = this.fb.group({
    id: [],
    fileName: [],
    missionId: [],
  });

  constructor(
    protected missionImageService: MissionImageService,
    protected missionService: MissionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ missionImage }) => {
      this.updateForm(missionImage);

      this.missionService.query().subscribe((res: HttpResponse<IMission[]>) => (this.missions = res.body || []));
    });
  }

  updateForm(missionImage: IMissionImage): void {
    this.editForm.patchValue({
      id: missionImage.id,
      fileName: missionImage.fileName,
      missionId: missionImage.missionId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const missionImage = this.createFromForm();
    if (missionImage.id !== undefined) {
      this.subscribeToSaveResponse(this.missionImageService.update(missionImage));
    } else {
      this.subscribeToSaveResponse(this.missionImageService.create(missionImage));
    }
  }

  private createFromForm(): IMissionImage {
    return {
      ...new MissionImage(),
      id: this.editForm.get(['id'])!.value,
      fileName: this.editForm.get(['fileName'])!.value,
      missionId: this.editForm.get(['missionId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMissionImage>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IMission): any {
    return item.id;
  }
}
