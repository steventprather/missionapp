import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMissionImage } from 'app/shared/model/missions/mission-image.model';
import { MissionImageService } from './mission-image.service';

@Component({
  templateUrl: './mission-image-delete-dialog.component.html',
})
export class MissionImageDeleteDialogComponent {
  missionImage?: IMissionImage;

  constructor(
    protected missionImageService: MissionImageService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.missionImageService.delete(id).subscribe(() => {
      this.eventManager.broadcast('missionImageListModification');
      this.activeModal.close();
    });
  }
}
