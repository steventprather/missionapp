import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMissionImage } from 'app/shared/model/missions/mission-image.model';

type EntityResponseType = HttpResponse<IMissionImage>;
type EntityArrayResponseType = HttpResponse<IMissionImage[]>;

@Injectable({ providedIn: 'root' })
export class MissionImageService {
  public resourceUrl = SERVER_API_URL + 'services/missions/api/mission-images';

  constructor(protected http: HttpClient) {}

  create(missionImage: IMissionImage): Observable<EntityResponseType> {
    return this.http.post<IMissionImage>(this.resourceUrl, missionImage, { observe: 'response' });
  }

  update(missionImage: IMissionImage): Observable<EntityResponseType> {
    return this.http.put<IMissionImage>(this.resourceUrl, missionImage, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMissionImage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMissionImage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
