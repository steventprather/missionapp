import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared/shared.module';
import { MissionImageComponent } from './mission-image.component';
import { MissionImageDetailComponent } from './mission-image-detail.component';
import { MissionImageUpdateComponent } from './mission-image-update.component';
import { MissionImageDeleteDialogComponent } from './mission-image-delete-dialog.component';
import { missionImageRoute } from './mission-image.route';

@NgModule({
  imports: [GatewaySharedModule, RouterModule.forChild(missionImageRoute)],
  declarations: [MissionImageComponent, MissionImageDetailComponent, MissionImageUpdateComponent, MissionImageDeleteDialogComponent],
  entryComponents: [MissionImageDeleteDialogComponent],
})
export class MissionsMissionImageModule {}
