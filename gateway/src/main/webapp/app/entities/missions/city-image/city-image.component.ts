import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICityImage } from 'app/shared/model/missions/city-image.model';
import { CityImageService } from './city-image.service';
import { CityImageDeleteDialogComponent } from './city-image-delete-dialog.component';

@Component({
  selector: 'jhi-city-image',
  templateUrl: './city-image.component.html',
})
export class CityImageComponent implements OnInit, OnDestroy {
  cityImages?: ICityImage[];
  eventSubscriber?: Subscription;

  constructor(protected cityImageService: CityImageService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.cityImageService.query().subscribe((res: HttpResponse<ICityImage[]>) => (this.cityImages = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCityImages();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICityImage): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCityImages(): void {
    this.eventSubscriber = this.eventManager.subscribe('cityImageListModification', () => this.loadAll());
  }

  delete(cityImage: ICityImage): void {
    const modalRef = this.modalService.open(CityImageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.cityImage = cityImage;
  }
}
