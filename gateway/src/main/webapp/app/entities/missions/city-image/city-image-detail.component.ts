import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICityImage } from 'app/shared/model/missions/city-image.model';

@Component({
  selector: 'jhi-city-image-detail',
  templateUrl: './city-image-detail.component.html',
})
export class CityImageDetailComponent implements OnInit {
  cityImage: ICityImage | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cityImage }) => (this.cityImage = cityImage));
  }

  previousState(): void {
    window.history.back();
  }
}
