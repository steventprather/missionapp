import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICityImage, CityImage } from 'app/shared/model/missions/city-image.model';
import { CityImageService } from './city-image.service';
import { CityImageComponent } from './city-image.component';
import { CityImageDetailComponent } from './city-image-detail.component';
import { CityImageUpdateComponent } from './city-image-update.component';

@Injectable({ providedIn: 'root' })
export class CityImageResolve implements Resolve<ICityImage> {
  constructor(private service: CityImageService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICityImage> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cityImage: HttpResponse<CityImage>) => {
          if (cityImage.body) {
            return of(cityImage.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CityImage());
  }
}

export const cityImageRoute: Routes = [
  {
    path: '',
    component: CityImageComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsCityImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CityImageDetailComponent,
    resolve: {
      cityImage: CityImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsCityImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CityImageUpdateComponent,
    resolve: {
      cityImage: CityImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsCityImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CityImageUpdateComponent,
    resolve: {
      cityImage: CityImageResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gatewayApp.missionsCityImage.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
