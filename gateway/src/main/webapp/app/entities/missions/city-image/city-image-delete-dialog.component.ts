import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICityImage } from 'app/shared/model/missions/city-image.model';
import { CityImageService } from './city-image.service';

@Component({
  templateUrl: './city-image-delete-dialog.component.html',
})
export class CityImageDeleteDialogComponent {
  cityImage?: ICityImage;

  constructor(protected cityImageService: CityImageService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cityImageService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cityImageListModification');
      this.activeModal.close();
    });
  }
}
