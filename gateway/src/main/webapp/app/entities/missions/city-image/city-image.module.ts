import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared/shared.module';
import { CityImageComponent } from './city-image.component';
import { CityImageDetailComponent } from './city-image-detail.component';
import { CityImageUpdateComponent } from './city-image-update.component';
import { CityImageDeleteDialogComponent } from './city-image-delete-dialog.component';
import { cityImageRoute } from './city-image.route';

@NgModule({
  imports: [GatewaySharedModule, RouterModule.forChild(cityImageRoute)],
  declarations: [CityImageComponent, CityImageDetailComponent, CityImageUpdateComponent, CityImageDeleteDialogComponent],
  entryComponents: [CityImageDeleteDialogComponent],
})
export class MissionsCityImageModule {}
