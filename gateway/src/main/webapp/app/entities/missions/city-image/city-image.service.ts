import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICityImage } from 'app/shared/model/missions/city-image.model';

type EntityResponseType = HttpResponse<ICityImage>;
type EntityArrayResponseType = HttpResponse<ICityImage[]>;

@Injectable({ providedIn: 'root' })
export class CityImageService {
  public resourceUrl = SERVER_API_URL + 'services/missions/api/city-images';

  constructor(protected http: HttpClient) {}

  create(cityImage: ICityImage): Observable<EntityResponseType> {
    return this.http.post<ICityImage>(this.resourceUrl, cityImage, { observe: 'response' });
  }

  update(cityImage: ICityImage): Observable<EntityResponseType> {
    return this.http.put<ICityImage>(this.resourceUrl, cityImage, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICityImage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICityImage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
