import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICityImage, CityImage } from 'app/shared/model/missions/city-image.model';
import { CityImageService } from './city-image.service';
import { ICity } from 'app/shared/model/missions/city.model';
import { CityService } from 'app/entities/missions/city/city.service';

@Component({
  selector: 'jhi-city-image-update',
  templateUrl: './city-image-update.component.html',
})
export class CityImageUpdateComponent implements OnInit {
  isSaving = false;
  cities: ICity[] = [];

  editForm = this.fb.group({
    id: [],
    fileName: [],
    cityId: [],
  });

  constructor(
    protected cityImageService: CityImageService,
    protected cityService: CityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cityImage }) => {
      this.updateForm(cityImage);

      this.cityService.query().subscribe((res: HttpResponse<ICity[]>) => (this.cities = res.body || []));
    });
  }

  updateForm(cityImage: ICityImage): void {
    this.editForm.patchValue({
      id: cityImage.id,
      fileName: cityImage.fileName,
      cityId: cityImage.cityId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cityImage = this.createFromForm();
    if (cityImage.id !== undefined) {
      this.subscribeToSaveResponse(this.cityImageService.update(cityImage));
    } else {
      this.subscribeToSaveResponse(this.cityImageService.create(cityImage));
    }
  }

  private createFromForm(): ICityImage {
    return {
      ...new CityImage(),
      id: this.editForm.get(['id'])!.value,
      fileName: this.editForm.get(['fileName'])!.value,
      cityId: this.editForm.get(['cityId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICityImage>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICity): any {
    return item.id;
  }
}
