import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'city',
        loadChildren: () => import('./missions/city/city.module').then(m => m.MissionsCityModule),
      },
      {
        path: 'city-image',
        loadChildren: () => import('./missions/city-image/city-image.module').then(m => m.MissionsCityImageModule),
      },
      {
        path: 'mission',
        loadChildren: () => import('./missions/mission/mission.module').then(m => m.MissionsMissionModule),
      },
      {
        path: 'mission-image',
        loadChildren: () => import('./missions/mission-image/mission-image.module').then(m => m.MissionsMissionImageModule),
      },
      {
        path: 'objective',
        loadChildren: () => import('./missions/objective/objective.module').then(m => m.MissionsObjectiveModule),
      },
      {
        path: 'objective-image',
        loadChildren: () => import('./missions/objective-image/objective-image.module').then(m => m.MissionsObjectiveImageModule),
      },
      {
        path: 'user-city',
        loadChildren: () => import('./responses/user-city/user-city.module').then(m => m.ResponsesUserCityModule),
      },
      {
        path: 'user-mission',
        loadChildren: () => import('./responses/user-mission/user-mission.module').then(m => m.ResponsesUserMissionModule),
      },
      {
        path: 'user-response',
        loadChildren: () => import('./responses/user-response/user-response.module').then(m => m.ResponsesUserResponseModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class GatewayEntityModule {}
