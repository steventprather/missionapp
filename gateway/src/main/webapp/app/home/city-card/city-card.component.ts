import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { faThumbsUp, faHeart, faShare, faEye, faDollarSign } from '@fortawesome/free-solid-svg-icons';
import { ICity } from 'app/shared/model/missions/city.model';
import { SubscriptionService } from 'app/shared/model/subscriptions/subscription.service';
import { SubscriptionRequest, SubscriptionTarget } from 'app/shared/model/subscriptions/subscription-request.dto';

@Component({
  selector: 'jhi-city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.scss']
})
export class CityCardComponent implements OnInit {

  @Input() city!: ICity;
  
  faThumbsUp = faThumbsUp;
  faHeart = faHeart;
  faShare = faShare;
  faEye = faEye;
  faDollarSign = faDollarSign;

  constructor(private router: Router, private subscriptionService: SubscriptionService) { }

  ngOnInit(): void {
  }

  subscribeToCity(city: ICity): void {
    const subscription = new SubscriptionRequest(SubscriptionTarget.City, city.id!, true);

    this.subscriptionService.subscribe(subscription).subscribe(result => {
      this.router.navigate(['/city'])
    });
  }
}
