import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { CityService } from 'app/entities/missions/city/city.service';
import { HttpResponse } from '@angular/common/http';
import { ICity } from 'app/shared/model/missions/city.model';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;

  cities: ICity[];

  constructor(private accountService: AccountService, 
    private loginModalService: LoginModalService,
    private cityService: CityService
  ) {

    this.cities = [];

  }

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.loadAllCities();
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  private loadAllCities(): void {
    this.cityService.query()
      .subscribe((res: HttpResponse<ICity[]>) => {
        const data = res.body;
        if (data) {
          this.cities = [];
          for (let i = 0; i < data.length; i++) {
            this.cities.push(data[i]);
          }
        }
      });
  }
}
