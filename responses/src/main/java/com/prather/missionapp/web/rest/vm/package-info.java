/**
 * View Models used by Spring MVC REST controllers.
 */
package com.prather.missionapp.web.rest.vm;
