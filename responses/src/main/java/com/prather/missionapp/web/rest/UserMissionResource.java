package com.prather.missionapp.web.rest;

import com.prather.missionapp.domain.dto.UserMissionDTO;
import com.prather.missionapp.service.UserMissionService;
import com.prather.missionapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.prather.missionapp.domain.UserMission}.
 */
@RestController
@RequestMapping("/api")
public class UserMissionResource {

    private final Logger log = LoggerFactory.getLogger(UserMissionResource.class);

    private static final String ENTITY_NAME = "responsesUserMission";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserMissionService userMissionService;

    public UserMissionResource(UserMissionService userMissionService) {
        this.userMissionService = userMissionService;
    }

    /**
     * {@code POST  /user-missions} : Create a new userMission.
     *
     * @param userMissionDTO the userMissionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userMissionDTO, or with status {@code 400 (Bad Request)} if the userMission has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-missions")
    public ResponseEntity<UserMissionDTO> createUserMission(@RequestBody UserMissionDTO userMissionDTO) throws URISyntaxException {
        log.debug("REST request to save UserMission : {}", userMissionDTO);
        if (userMissionDTO.getId() != null) {
            throw new BadRequestAlertException("A new userMission cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserMissionDTO result = userMissionService.save(userMissionDTO);
        return ResponseEntity.created(new URI("/api/user-missions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-missions} : Updates an existing userMission.
     *
     * @param userMissionDTO the userMissionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userMissionDTO,
     * or with status {@code 400 (Bad Request)} if the userMissionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userMissionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-missions")
    public ResponseEntity<UserMissionDTO> updateUserMission(@RequestBody UserMissionDTO userMissionDTO) throws URISyntaxException {
        log.debug("REST request to update UserMission : {}", userMissionDTO);
        if (userMissionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserMissionDTO result = userMissionService.save(userMissionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userMissionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-missions} : get all the userMissions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userMissions in body.
     */
    @GetMapping("/user-missions")
    public ResponseEntity<List<UserMissionDTO>> getAllUserMissions(Pageable pageable) {
        log.debug("REST request to get a page of UserMissions");
        Page<UserMissionDTO> page = userMissionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-missions/:id} : get the "id" userMission.
     *
     * @param id the id of the userMissionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userMissionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-missions/{id}")
    public ResponseEntity<UserMissionDTO> getUserMission(@PathVariable Long id) {
        log.debug("REST request to get UserMission : {}", id);
        Optional<UserMissionDTO> userMissionDTO = userMissionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userMissionDTO);
    }

    /**
     * {@code DELETE  /user-missions/:id} : delete the "id" userMission.
     *
     * @param id the id of the userMissionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-missions/{id}")
    public ResponseEntity<Void> deleteUserMission(@PathVariable Long id) {
        log.debug("REST request to delete UserMission : {}", id);

        userMissionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
