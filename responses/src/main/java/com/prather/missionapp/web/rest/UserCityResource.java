package com.prather.missionapp.web.rest;

import com.prather.missionapp.domain.dto.UserCityDTO;
import com.prather.missionapp.service.UserCityService;
import com.prather.missionapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.prather.missionapp.domain.UserCity}.
 */
@RestController
@RequestMapping("/api")
public class UserCityResource {

    private final Logger log = LoggerFactory.getLogger(UserCityResource.class);

    private static final String ENTITY_NAME = "responsesUserCity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserCityService userCityService;

    public UserCityResource(UserCityService userCityService) {
        this.userCityService = userCityService;
    }

    /**
     * {@code POST  /user-cities} : Create a new userCity.
     *
     * @param userCityDTO the userCityDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userCityDTO, or with status {@code 400 (Bad Request)} if the userCity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-cities")
    public ResponseEntity<UserCityDTO> createUserCity(@RequestBody UserCityDTO userCityDTO) throws URISyntaxException {
        log.debug("REST request to save UserCity : {}", userCityDTO);
        if (userCityDTO.getId() != null) {
            throw new BadRequestAlertException("A new userCity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserCityDTO result = userCityService.save(userCityDTO);
        return ResponseEntity.created(new URI("/api/user-cities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-cities} : Updates an existing userCity.
     *
     * @param userCityDTO the userCityDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userCityDTO,
     * or with status {@code 400 (Bad Request)} if the userCityDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userCityDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-cities")
    public ResponseEntity<UserCityDTO> updateUserCity(@RequestBody UserCityDTO userCityDTO) throws URISyntaxException {
        log.debug("REST request to update UserCity : {}", userCityDTO);
        if (userCityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserCityDTO result = userCityService.save(userCityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userCityDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-cities} : get all the userCities.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userCities in body.
     */
    @GetMapping("/user-cities")
    public ResponseEntity<List<UserCityDTO>> getAllUserCities(Pageable pageable) {
        log.debug("REST request to get a page of UserCities");
        Page<UserCityDTO> page = userCityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-cities/:id} : get the "id" userCity.
     *
     * @param id the id of the userCityDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userCityDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-cities/{id}")
    public ResponseEntity<UserCityDTO> getUserCity(@PathVariable Long id) {
        log.debug("REST request to get UserCity : {}", id);
        Optional<UserCityDTO> userCityDTO = userCityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userCityDTO);
    }

    /**
     * {@code DELETE  /user-cities/:id} : delete the "id" userCity.
     *
     * @param id the id of the userCityDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-cities/{id}")
    public ResponseEntity<Void> deleteUserCity(@PathVariable Long id) {
        log.debug("REST request to delete UserCity : {}", id);

        userCityService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
