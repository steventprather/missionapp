package com.prather.missionapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prather.missionapp.service.SubscriptionService;
import com.prather.missionapp.service.dto.SubscriptionRequestDTO;

import io.github.jhipster.web.util.HeaderUtil;

/**
 * SubscriptionResource controller
 */
@RestController
@RequestMapping("/api/subscription")
public class SubscriptionResource {

    private final Logger log = LoggerFactory.getLogger(SubscriptionResource.class);

    private static final String ENTITY_NAME = "subscription";
    
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    
    private final SubscriptionService subscriptionService;

    public SubscriptionResource(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }
    
    /**
    * GET defaultAction
    */
    @GetMapping("/default-action")
    public String defaultAction() {
        return "defaultAction";
    }

    @PostMapping
    public ResponseEntity<SubscriptionRequestDTO> subscribe(@RequestBody SubscriptionRequestDTO request) throws URISyntaxException {
    	log.debug("subscribe to city: {} {} {}", request.getTarget().name(), request.getId(), request.isSubscribed());
    	
    	subscriptionService.subscribe(request);
    	
    	return ResponseEntity.created(new URI("/api/subscription/" + request.getId()))
    		.headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, request.getId().toString()))
    		.body(request);  	
    	
    }
}
