package com.prather.missionapp.service.mapper;


import com.prather.missionapp.domain.*;
import com.prather.missionapp.domain.dto.UserCityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserCity} and its DTO {@link UserCityDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserCityMapper extends EntityMapper<UserCityDTO, UserCity> {



    default UserCity fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserCity userCity = new UserCity();
        userCity.setId(id);
        return userCity;
    }
}
