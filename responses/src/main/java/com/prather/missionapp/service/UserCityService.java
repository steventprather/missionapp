package com.prather.missionapp.service;

import com.prather.missionapp.domain.UserCity;
import com.prather.missionapp.domain.dto.UserCityDTO;
import com.prather.missionapp.repository.UserCityRepository;
import com.prather.missionapp.service.mapper.UserCityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UserCity}.
 */
@Service
@Transactional
public class UserCityService {

    private final Logger log = LoggerFactory.getLogger(UserCityService.class);

    private final UserCityRepository userCityRepository;

    private final UserCityMapper userCityMapper;

    public UserCityService(UserCityRepository userCityRepository, UserCityMapper userCityMapper) {
        this.userCityRepository = userCityRepository;
        this.userCityMapper = userCityMapper;
    }

    /**
     * Save a userCity.
     *
     * @param userCityDTO the entity to save.
     * @return the persisted entity.
     */
    public UserCityDTO save(UserCityDTO userCityDTO) {
        log.debug("Request to save UserCity : {}", userCityDTO);
        UserCity userCity = userCityMapper.toEntity(userCityDTO);
        userCity = userCityRepository.save(userCity);
        return userCityMapper.toDto(userCity);
    }

    /**
     * Get all the userCities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<UserCityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserCities");
        return userCityRepository.findAll(pageable)
            .map(userCityMapper::toDto);
    }


    /**
     * Get one userCity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UserCityDTO> findOne(Long id) {
        log.debug("Request to get UserCity : {}", id);
        return userCityRepository.findById(id)
            .map(userCityMapper::toDto);
    }

    /**
     * Delete the userCity by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UserCity : {}", id);

        userCityRepository.deleteById(id);
    }
}
