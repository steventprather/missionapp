package com.prather.missionapp.service.dto;

public enum SubscriptionTarget {
	City,
    Mission
}
