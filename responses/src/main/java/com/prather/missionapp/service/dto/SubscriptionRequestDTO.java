package com.prather.missionapp.service.dto;

public class SubscriptionRequestDTO {
	private SubscriptionTarget target;
    private Long id;
    private boolean subscribed;

    public SubscriptionRequestDTO() {
    }
    
    public SubscriptionRequestDTO(SubscriptionTarget target, Long id, boolean subscribed) {
        this.target = target;
        this.id = id;
        this.subscribed = subscribed;
    }

    public SubscriptionTarget getTarget() {
        return target;
    }

    public void setTarget(SubscriptionTarget target) {
        this.target = target;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }
}
