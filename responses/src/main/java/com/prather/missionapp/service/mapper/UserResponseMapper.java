package com.prather.missionapp.service.mapper;


import com.prather.missionapp.domain.*;
import com.prather.missionapp.domain.dto.UserResponseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserResponse} and its DTO {@link UserResponseDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMissionMapper.class})
public interface UserResponseMapper extends EntityMapper<UserResponseDTO, UserResponse> {

    @Mapping(source = "userMission.id", target = "userMissionId")
    @Mapping(source = "parent.id", target = "parentId")
    UserResponseDTO toDto(UserResponse userResponse);

    @Mapping(source = "userMissionId", target = "userMission")
    @Mapping(source = "parentId", target = "parent")
    UserResponse toEntity(UserResponseDTO userResponseDTO);

    default UserResponse fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserResponse userResponse = new UserResponse();
        userResponse.setId(id);
        return userResponse;
    }
}
