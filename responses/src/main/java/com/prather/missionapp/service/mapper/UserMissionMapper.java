package com.prather.missionapp.service.mapper;


import com.prather.missionapp.domain.*;
import com.prather.missionapp.domain.dto.UserMissionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserMission} and its DTO {@link UserMissionDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserCityMapper.class})
public interface UserMissionMapper extends EntityMapper<UserMissionDTO, UserMission> {

    @Mapping(source = "userCity.id", target = "userCityId")
    UserMissionDTO toDto(UserMission userMission);

    @Mapping(source = "userCityId", target = "userCity")
    UserMission toEntity(UserMissionDTO userMissionDTO);

    default UserMission fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserMission userMission = new UserMission();
        userMission.setId(id);
        return userMission;
    }
}
