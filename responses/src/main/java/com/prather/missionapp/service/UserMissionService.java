package com.prather.missionapp.service;

import com.prather.missionapp.domain.UserMission;
import com.prather.missionapp.domain.dto.UserMissionDTO;
import com.prather.missionapp.repository.UserMissionRepository;
import com.prather.missionapp.service.mapper.UserMissionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UserMission}.
 */
@Service
@Transactional
public class UserMissionService {

    private final Logger log = LoggerFactory.getLogger(UserMissionService.class);

    private final UserMissionRepository userMissionRepository;

    private final UserMissionMapper userMissionMapper;

    public UserMissionService(UserMissionRepository userMissionRepository, UserMissionMapper userMissionMapper) {
        this.userMissionRepository = userMissionRepository;
        this.userMissionMapper = userMissionMapper;
    }

    /**
     * Save a userMission.
     *
     * @param userMissionDTO the entity to save.
     * @return the persisted entity.
     */
    public UserMissionDTO save(UserMissionDTO userMissionDTO) {
        log.debug("Request to save UserMission : {}", userMissionDTO);
        UserMission userMission = userMissionMapper.toEntity(userMissionDTO);
        userMission = userMissionRepository.save(userMission);
        return userMissionMapper.toDto(userMission);
    }

    /**
     * Get all the userMissions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<UserMissionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserMissions");
        return userMissionRepository.findAll(pageable)
            .map(userMissionMapper::toDto);
    }


    /**
     * Get one userMission by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UserMissionDTO> findOne(Long id) {
        log.debug("Request to get UserMission : {}", id);
        return userMissionRepository.findById(id)
            .map(userMissionMapper::toDto);
    }

    /**
     * Delete the userMission by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UserMission : {}", id);

        userMissionRepository.deleteById(id);
    }
}
