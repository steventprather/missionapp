package com.prather.missionapp.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prather.missionapp.domain.UserCity;
import com.prather.missionapp.domain.UserMission;
import com.prather.missionapp.domain.UserResponse;
import com.prather.missionapp.domain.dto.MissionDTO;
import com.prather.missionapp.domain.dto.ObjectiveDTO;
import com.prather.missionapp.gateway.client.UserClient;
import com.prather.missionapp.missions.client.MissionClient;
import com.prather.missionapp.missions.client.ObjectiveClient;
import com.prather.missionapp.repository.UserCityRepository;
import com.prather.missionapp.repository.UserMissionRepository;
import com.prather.missionapp.repository.UserResponseRepository;
import com.prather.missionapp.service.dto.SubscriptionRequestDTO;

@Service
@Transactional
public class SubscriptionService {

	private final Logger log = LoggerFactory.getLogger(SubscriptionService.class);

    private final UserCityRepository userCityRepository;
    
    private final UserMissionRepository userMissionRepository;
    
    private final UserResponseRepository userResponseRepository;
    
    private final UserClient userClient;

    private final MissionClient missionClient;
    
    private final ObjectiveClient objectiveClient;
    
    public SubscriptionService(UserCityRepository userCityRepository, 
    		UserMissionRepository userMissionRepository, UserResponseRepository userResponseRepository,
    		UserClient userClient, MissionClient missionClient, ObjectiveClient objectiveClient) {
        this.userCityRepository = userCityRepository;
        this.userMissionRepository = userMissionRepository;
        this.userResponseRepository = userResponseRepository;
        this.userClient = userClient;
        this.missionClient = missionClient;
        this.objectiveClient = objectiveClient;
        
    }

    public SubscriptionRequestDTO subscribe(SubscriptionRequestDTO request) {

    	log.info("check user service");
    	SecurityContext securityContext = SecurityContextHolder.getContext();
    	String login = securityContext.getAuthentication().getName();
    	log.info("user: {}", login);
    	
        UserCity userCity = userCityRepository.findByUserIdAndCityId(login, request.getId());
        if (userCity == null) {
        	createSubscription(request, login);
        } else {
        	//TODO what do we do if the user is already subscribed. Update or leave it alone?
        	
        }
                
        return request;
    }
    
    private void createSubscription(SubscriptionRequestDTO request, String login) {
    	
    	UserCity userCity = new UserCity();
    	userCity.setCityId(request.getId());
    	userCity.setUserId(login);
    	final UserCity fuserCity = userCityRepository.save(userCity);
    	
    	List<MissionDTO> missions = missionClient.getByCity(request.getId());
        log.info("missions");
        log.info(missions.toString());
        
        missions.forEach(m -> {
        	UserMission userMission = new UserMission();
            userMission.setMissionId(m.getId());
            userMission.setUserId(login);
            userMission.setUserCity(fuserCity);
            final UserMission fuserMission = userMissionRepository.save(userMission);
            
            // TODO make sure this is ordered by order so we can guarantee parents are before children
            List<ObjectiveDTO> objectives = objectiveClient.getByMission(m.getId());
            log.info("objectives");
            log.info(objectives.toString());
            
            objectives.forEach(o -> {
            	UserResponse userResponse = new UserResponse();
            	userResponse.setChecked(false);
            	userResponse.setItems(0);
            	userResponse.setObjectiveId(o.getId());
            	userResponse.setPoints(0);
            	userResponse.setUserMission(fuserMission);
            	
            	log.info("objective: {}", o.getId());
            	if (o.getParentId() != null) {
            		log.info("objective parent: {}", o.getParentId());
            		UserResponse parentResponse = userResponseRepository.findByUserIdAndObjectiveId(login, o.getParentId());
            		log.info("found parent: {}", parentResponse == null ? "null" : parentResponse.getId());
            		userResponse.setParent(parentResponse);
            	}
            	
            	userResponse = userResponseRepository.save(userResponse);
            });
        });
            	
    }

}
