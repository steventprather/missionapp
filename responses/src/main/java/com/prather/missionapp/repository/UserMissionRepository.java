package com.prather.missionapp.repository;

import com.prather.missionapp.domain.UserMission;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the UserMission entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserMissionRepository extends JpaRepository<UserMission, Long> {
}
