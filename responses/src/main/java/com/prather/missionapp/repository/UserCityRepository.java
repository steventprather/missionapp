package com.prather.missionapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prather.missionapp.domain.UserCity;

/**
 * Spring Data  repository for the UserCity entity.
 */
/**
 * @author steve
 *
 */
@SuppressWarnings("unused")
@Repository
public interface UserCityRepository extends JpaRepository<UserCity, Long> {
	
	/**
	 * 
	 * @param userId
	 * This userId is actually the login, due to a mistake I made. The generator creates the Id property of the User as 
	 * an integer if you use the standard authentication mechanism. It creates the Id property as a String if you use Okta.
	 * I was playing with both and assumed they were the same. So, when I created the model for this project, I used
	 * String as the user id type. I was wrong.
	 * 
	 * However, it's not that big of a deal. The login property is unique so I can use it. And, it's not going to be such a large 
	 * database that storage will be an issue. I would normally fix this if I were working on a project with a larger scale.  
	 *  
	 * @return
	 */
	List<UserCity> findByUserId(String userId);
	
	
	/**
	 * @param userId
	 * This userId is actually the login, due to a mistake I made. The generator creates the Id property of the User as 
	 * an integer if you use the standard authentication mechanism. It creates the Id property as a String if you use Okta.
	 * I was playing with both and assumed they were the same. So, when I created the model for this project, I used
	 * String as the user id type. I was wrong.
	 * 
	 * However, it's not that big of a deal. The login property is unique so I can use it. And, it's not going to be such a large 
	 * database that storage will be an issue. I would normally fix this if I were working on a project with a larger scale.
	 * 
	 * @param cityId
	 * @return
	 */
	UserCity findByUserIdAndCityId(String userId, Long cityId);
	
}
