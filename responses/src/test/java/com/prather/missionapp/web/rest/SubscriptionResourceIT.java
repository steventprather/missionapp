package com.prather.missionapp.web.rest;

import com.prather.missionapp.ResponsesApp;
import com.prather.missionapp.service.SubscriptionService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the SubscriptionResource REST controller.
 *
 * @see SubscriptionResource
 */
@SpringBootTest(classes = ResponsesApp.class)
public class SubscriptionResourceIT {

    private MockMvc restMockMvc;

    @Autowired
    private SubscriptionService subscriptionService;
    
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        SubscriptionResource subscriptionResource = new SubscriptionResource(subscriptionService);
        restMockMvc = MockMvcBuilders
            .standaloneSetup(subscriptionResource)
            .build();
    }

    /**
     * Test defaultAction
     */
    @Test
    public void testDefaultAction() throws Exception {
        restMockMvc.perform(get("/api/subscription/default-action"))
            .andExpect(status().isOk());
    }
}
