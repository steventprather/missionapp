package com.prather.missionapp.web.rest;

import com.prather.missionapp.ResponsesApp;
import com.prather.missionapp.domain.UserMission;
import com.prather.missionapp.domain.dto.UserMissionDTO;
import com.prather.missionapp.repository.UserMissionRepository;
import com.prather.missionapp.service.UserMissionService;
import com.prather.missionapp.service.mapper.UserMissionMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserMissionResource} REST controller.
 */
@SpringBootTest(classes = ResponsesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class UserMissionResourceIT {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_MISSION_ID = 1L;
    private static final Long UPDATED_MISSION_ID = 2L;

    @Autowired
    private UserMissionRepository userMissionRepository;

    @Autowired
    private UserMissionMapper userMissionMapper;

    @Autowired
    private UserMissionService userMissionService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserMissionMockMvc;

    private UserMission userMission;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserMission createEntity(EntityManager em) {
        UserMission userMission = new UserMission()
            .userId(DEFAULT_USER_ID)
            .missionId(DEFAULT_MISSION_ID);
        return userMission;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserMission createUpdatedEntity(EntityManager em) {
        UserMission userMission = new UserMission()
            .userId(UPDATED_USER_ID)
            .missionId(UPDATED_MISSION_ID);
        return userMission;
    }

    @BeforeEach
    public void initTest() {
        userMission = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserMission() throws Exception {
        int databaseSizeBeforeCreate = userMissionRepository.findAll().size();
        // Create the UserMission
        UserMissionDTO userMissionDTO = userMissionMapper.toDto(userMission);
        restUserMissionMockMvc.perform(post("/api/user-missions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userMissionDTO)))
            .andExpect(status().isCreated());

        // Validate the UserMission in the database
        List<UserMission> userMissionList = userMissionRepository.findAll();
        assertThat(userMissionList).hasSize(databaseSizeBeforeCreate + 1);
        UserMission testUserMission = userMissionList.get(userMissionList.size() - 1);
        assertThat(testUserMission.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testUserMission.getMissionId()).isEqualTo(DEFAULT_MISSION_ID);
    }

    @Test
    @Transactional
    public void createUserMissionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userMissionRepository.findAll().size();

        // Create the UserMission with an existing ID
        userMission.setId(1L);
        UserMissionDTO userMissionDTO = userMissionMapper.toDto(userMission);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserMissionMockMvc.perform(post("/api/user-missions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userMissionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserMission in the database
        List<UserMission> userMissionList = userMissionRepository.findAll();
        assertThat(userMissionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserMissions() throws Exception {
        // Initialize the database
        userMissionRepository.saveAndFlush(userMission);

        // Get all the userMissionList
        restUserMissionMockMvc.perform(get("/api/user-missions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userMission.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].missionId").value(hasItem(DEFAULT_MISSION_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getUserMission() throws Exception {
        // Initialize the database
        userMissionRepository.saveAndFlush(userMission);

        // Get the userMission
        restUserMissionMockMvc.perform(get("/api/user-missions/{id}", userMission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userMission.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.missionId").value(DEFAULT_MISSION_ID.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingUserMission() throws Exception {
        // Get the userMission
        restUserMissionMockMvc.perform(get("/api/user-missions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserMission() throws Exception {
        // Initialize the database
        userMissionRepository.saveAndFlush(userMission);

        int databaseSizeBeforeUpdate = userMissionRepository.findAll().size();

        // Update the userMission
        UserMission updatedUserMission = userMissionRepository.findById(userMission.getId()).get();
        // Disconnect from session so that the updates on updatedUserMission are not directly saved in db
        em.detach(updatedUserMission);
        updatedUserMission
            .userId(UPDATED_USER_ID)
            .missionId(UPDATED_MISSION_ID);
        UserMissionDTO userMissionDTO = userMissionMapper.toDto(updatedUserMission);

        restUserMissionMockMvc.perform(put("/api/user-missions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userMissionDTO)))
            .andExpect(status().isOk());

        // Validate the UserMission in the database
        List<UserMission> userMissionList = userMissionRepository.findAll();
        assertThat(userMissionList).hasSize(databaseSizeBeforeUpdate);
        UserMission testUserMission = userMissionList.get(userMissionList.size() - 1);
        assertThat(testUserMission.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testUserMission.getMissionId()).isEqualTo(UPDATED_MISSION_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingUserMission() throws Exception {
        int databaseSizeBeforeUpdate = userMissionRepository.findAll().size();

        // Create the UserMission
        UserMissionDTO userMissionDTO = userMissionMapper.toDto(userMission);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserMissionMockMvc.perform(put("/api/user-missions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userMissionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserMission in the database
        List<UserMission> userMissionList = userMissionRepository.findAll();
        assertThat(userMissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserMission() throws Exception {
        // Initialize the database
        userMissionRepository.saveAndFlush(userMission);

        int databaseSizeBeforeDelete = userMissionRepository.findAll().size();

        // Delete the userMission
        restUserMissionMockMvc.perform(delete("/api/user-missions/{id}", userMission.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserMission> userMissionList = userMissionRepository.findAll();
        assertThat(userMissionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
