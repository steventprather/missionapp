package com.prather.missionapp.web.rest;

import com.prather.missionapp.ResponsesApp;
import com.prather.missionapp.domain.UserCity;
import com.prather.missionapp.domain.dto.UserCityDTO;
import com.prather.missionapp.repository.UserCityRepository;
import com.prather.missionapp.service.UserCityService;
import com.prather.missionapp.service.mapper.UserCityMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserCityResource} REST controller.
 */
@SpringBootTest(classes = ResponsesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class UserCityResourceIT {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_CITY_ID = 1L;
    private static final Long UPDATED_CITY_ID = 2L;

    @Autowired
    private UserCityRepository userCityRepository;

    @Autowired
    private UserCityMapper userCityMapper;

    @Autowired
    private UserCityService userCityService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserCityMockMvc;

    private UserCity userCity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserCity createEntity(EntityManager em) {
        UserCity userCity = new UserCity()
            .userId(DEFAULT_USER_ID)
            .cityId(DEFAULT_CITY_ID);
        return userCity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserCity createUpdatedEntity(EntityManager em) {
        UserCity userCity = new UserCity()
            .userId(UPDATED_USER_ID)
            .cityId(UPDATED_CITY_ID);
        return userCity;
    }

    @BeforeEach
    public void initTest() {
        userCity = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserCity() throws Exception {
        int databaseSizeBeforeCreate = userCityRepository.findAll().size();
        // Create the UserCity
        UserCityDTO userCityDTO = userCityMapper.toDto(userCity);
        restUserCityMockMvc.perform(post("/api/user-cities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userCityDTO)))
            .andExpect(status().isCreated());

        // Validate the UserCity in the database
        List<UserCity> userCityList = userCityRepository.findAll();
        assertThat(userCityList).hasSize(databaseSizeBeforeCreate + 1);
        UserCity testUserCity = userCityList.get(userCityList.size() - 1);
        assertThat(testUserCity.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testUserCity.getCityId()).isEqualTo(DEFAULT_CITY_ID);
    }

    @Test
    @Transactional
    public void createUserCityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userCityRepository.findAll().size();

        // Create the UserCity with an existing ID
        userCity.setId(1L);
        UserCityDTO userCityDTO = userCityMapper.toDto(userCity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserCityMockMvc.perform(post("/api/user-cities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userCityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserCity in the database
        List<UserCity> userCityList = userCityRepository.findAll();
        assertThat(userCityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserCities() throws Exception {
        // Initialize the database
        userCityRepository.saveAndFlush(userCity);

        // Get all the userCityList
        restUserCityMockMvc.perform(get("/api/user-cities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userCity.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getUserCity() throws Exception {
        // Initialize the database
        userCityRepository.saveAndFlush(userCity);

        // Get the userCity
        restUserCityMockMvc.perform(get("/api/user-cities/{id}", userCity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userCity.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.cityId").value(DEFAULT_CITY_ID.intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingUserCity() throws Exception {
        // Get the userCity
        restUserCityMockMvc.perform(get("/api/user-cities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserCity() throws Exception {
        // Initialize the database
        userCityRepository.saveAndFlush(userCity);

        int databaseSizeBeforeUpdate = userCityRepository.findAll().size();

        // Update the userCity
        UserCity updatedUserCity = userCityRepository.findById(userCity.getId()).get();
        // Disconnect from session so that the updates on updatedUserCity are not directly saved in db
        em.detach(updatedUserCity);
        updatedUserCity
            .userId(UPDATED_USER_ID)
            .cityId(UPDATED_CITY_ID);
        UserCityDTO userCityDTO = userCityMapper.toDto(updatedUserCity);

        restUserCityMockMvc.perform(put("/api/user-cities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userCityDTO)))
            .andExpect(status().isOk());

        // Validate the UserCity in the database
        List<UserCity> userCityList = userCityRepository.findAll();
        assertThat(userCityList).hasSize(databaseSizeBeforeUpdate);
        UserCity testUserCity = userCityList.get(userCityList.size() - 1);
        assertThat(testUserCity.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testUserCity.getCityId()).isEqualTo(UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingUserCity() throws Exception {
        int databaseSizeBeforeUpdate = userCityRepository.findAll().size();

        // Create the UserCity
        UserCityDTO userCityDTO = userCityMapper.toDto(userCity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserCityMockMvc.perform(put("/api/user-cities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userCityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserCity in the database
        List<UserCity> userCityList = userCityRepository.findAll();
        assertThat(userCityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserCity() throws Exception {
        // Initialize the database
        userCityRepository.saveAndFlush(userCity);

        int databaseSizeBeforeDelete = userCityRepository.findAll().size();

        // Delete the userCity
        restUserCityMockMvc.perform(delete("/api/user-cities/{id}", userCity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserCity> userCityList = userCityRepository.findAll();
        assertThat(userCityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
