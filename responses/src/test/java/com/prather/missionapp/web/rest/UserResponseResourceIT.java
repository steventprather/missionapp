package com.prather.missionapp.web.rest;

import com.prather.missionapp.ResponsesApp;
import com.prather.missionapp.domain.UserResponse;
import com.prather.missionapp.domain.dto.UserResponseDTO;
import com.prather.missionapp.repository.UserResponseRepository;
import com.prather.missionapp.service.UserResponseService;
import com.prather.missionapp.service.mapper.UserResponseMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserResponseResource} REST controller.
 */
@SpringBootTest(classes = ResponsesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class UserResponseResourceIT {

    private static final Long DEFAULT_OBJECTIVE_ID = 1L;
    private static final Long UPDATED_OBJECTIVE_ID = 2L;

    private static final Boolean DEFAULT_CHECKED = false;
    private static final Boolean UPDATED_CHECKED = true;

    private static final Integer DEFAULT_ITEMS = 1;
    private static final Integer UPDATED_ITEMS = 2;

    private static final Integer DEFAULT_POINTS = 1;
    private static final Integer UPDATED_POINTS = 2;

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    @Autowired
    private UserResponseRepository userResponseRepository;

    @Autowired
    private UserResponseMapper userResponseMapper;

    @Autowired
    private UserResponseService userResponseService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserResponseMockMvc;

    private UserResponse userResponse;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserResponse createEntity(EntityManager em) {
        UserResponse userResponse = new UserResponse()
            .objectiveId(DEFAULT_OBJECTIVE_ID)
            .checked(DEFAULT_CHECKED)
            .items(DEFAULT_ITEMS)
            .points(DEFAULT_POINTS)
            .text(DEFAULT_TEXT);
        return userResponse;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserResponse createUpdatedEntity(EntityManager em) {
        UserResponse userResponse = new UserResponse()
            .objectiveId(UPDATED_OBJECTIVE_ID)
            .checked(UPDATED_CHECKED)
            .items(UPDATED_ITEMS)
            .points(UPDATED_POINTS)
            .text(UPDATED_TEXT);
        return userResponse;
    }

    @BeforeEach
    public void initTest() {
        userResponse = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserResponse() throws Exception {
        int databaseSizeBeforeCreate = userResponseRepository.findAll().size();
        // Create the UserResponse
        UserResponseDTO userResponseDTO = userResponseMapper.toDto(userResponse);
        restUserResponseMockMvc.perform(post("/api/user-responses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userResponseDTO)))
            .andExpect(status().isCreated());

        // Validate the UserResponse in the database
        List<UserResponse> userResponseList = userResponseRepository.findAll();
        assertThat(userResponseList).hasSize(databaseSizeBeforeCreate + 1);
        UserResponse testUserResponse = userResponseList.get(userResponseList.size() - 1);
        assertThat(testUserResponse.getObjectiveId()).isEqualTo(DEFAULT_OBJECTIVE_ID);
        assertThat(testUserResponse.isChecked()).isEqualTo(DEFAULT_CHECKED);
        assertThat(testUserResponse.getItems()).isEqualTo(DEFAULT_ITEMS);
        assertThat(testUserResponse.getPoints()).isEqualTo(DEFAULT_POINTS);
        assertThat(testUserResponse.getText()).isEqualTo(DEFAULT_TEXT);
    }

    @Test
    @Transactional
    public void createUserResponseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userResponseRepository.findAll().size();

        // Create the UserResponse with an existing ID
        userResponse.setId(1L);
        UserResponseDTO userResponseDTO = userResponseMapper.toDto(userResponse);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserResponseMockMvc.perform(post("/api/user-responses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userResponseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserResponse in the database
        List<UserResponse> userResponseList = userResponseRepository.findAll();
        assertThat(userResponseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserResponses() throws Exception {
        // Initialize the database
        userResponseRepository.saveAndFlush(userResponse);

        // Get all the userResponseList
        restUserResponseMockMvc.perform(get("/api/user-responses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].objectiveId").value(hasItem(DEFAULT_OBJECTIVE_ID.intValue())))
            .andExpect(jsonPath("$.[*].checked").value(hasItem(DEFAULT_CHECKED.booleanValue())))
            .andExpect(jsonPath("$.[*].items").value(hasItem(DEFAULT_ITEMS)))
            .andExpect(jsonPath("$.[*].points").value(hasItem(DEFAULT_POINTS)))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT)));
    }
    
    @Test
    @Transactional
    public void getUserResponse() throws Exception {
        // Initialize the database
        userResponseRepository.saveAndFlush(userResponse);

        // Get the userResponse
        restUserResponseMockMvc.perform(get("/api/user-responses/{id}", userResponse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userResponse.getId().intValue()))
            .andExpect(jsonPath("$.objectiveId").value(DEFAULT_OBJECTIVE_ID.intValue()))
            .andExpect(jsonPath("$.checked").value(DEFAULT_CHECKED.booleanValue()))
            .andExpect(jsonPath("$.items").value(DEFAULT_ITEMS))
            .andExpect(jsonPath("$.points").value(DEFAULT_POINTS))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT));
    }
    @Test
    @Transactional
    public void getNonExistingUserResponse() throws Exception {
        // Get the userResponse
        restUserResponseMockMvc.perform(get("/api/user-responses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserResponse() throws Exception {
        // Initialize the database
        userResponseRepository.saveAndFlush(userResponse);

        int databaseSizeBeforeUpdate = userResponseRepository.findAll().size();

        // Update the userResponse
        UserResponse updatedUserResponse = userResponseRepository.findById(userResponse.getId()).get();
        // Disconnect from session so that the updates on updatedUserResponse are not directly saved in db
        em.detach(updatedUserResponse);
        updatedUserResponse
            .objectiveId(UPDATED_OBJECTIVE_ID)
            .checked(UPDATED_CHECKED)
            .items(UPDATED_ITEMS)
            .points(UPDATED_POINTS)
            .text(UPDATED_TEXT);
        UserResponseDTO userResponseDTO = userResponseMapper.toDto(updatedUserResponse);

        restUserResponseMockMvc.perform(put("/api/user-responses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userResponseDTO)))
            .andExpect(status().isOk());

        // Validate the UserResponse in the database
        List<UserResponse> userResponseList = userResponseRepository.findAll();
        assertThat(userResponseList).hasSize(databaseSizeBeforeUpdate);
        UserResponse testUserResponse = userResponseList.get(userResponseList.size() - 1);
        assertThat(testUserResponse.getObjectiveId()).isEqualTo(UPDATED_OBJECTIVE_ID);
        assertThat(testUserResponse.isChecked()).isEqualTo(UPDATED_CHECKED);
        assertThat(testUserResponse.getItems()).isEqualTo(UPDATED_ITEMS);
        assertThat(testUserResponse.getPoints()).isEqualTo(UPDATED_POINTS);
        assertThat(testUserResponse.getText()).isEqualTo(UPDATED_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingUserResponse() throws Exception {
        int databaseSizeBeforeUpdate = userResponseRepository.findAll().size();

        // Create the UserResponse
        UserResponseDTO userResponseDTO = userResponseMapper.toDto(userResponse);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserResponseMockMvc.perform(put("/api/user-responses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userResponseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserResponse in the database
        List<UserResponse> userResponseList = userResponseRepository.findAll();
        assertThat(userResponseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserResponse() throws Exception {
        // Initialize the database
        userResponseRepository.saveAndFlush(userResponse);

        int databaseSizeBeforeDelete = userResponseRepository.findAll().size();

        // Delete the userResponse
        restUserResponseMockMvc.perform(delete("/api/user-responses/{id}", userResponse.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserResponse> userResponseList = userResponseRepository.findAll();
        assertThat(userResponseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
