package com.prather.missionapp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UserCityMapperTest {

    private UserCityMapper userCityMapper;

    @BeforeEach
    public void setUp() {
        userCityMapper = new UserCityMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(userCityMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(userCityMapper.fromId(null)).isNull();
    }
}
