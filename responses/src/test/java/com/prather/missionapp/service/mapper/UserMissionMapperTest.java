package com.prather.missionapp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UserMissionMapperTest {

    private UserMissionMapper userMissionMapper;

    @BeforeEach
    public void setUp() {
        userMissionMapper = new UserMissionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(userMissionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(userMissionMapper.fromId(null)).isNull();
    }
}
