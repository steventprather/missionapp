package com.prather.missionapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import com.prather.missionapp.domain.dto.UserMissionDTO;
import com.prather.missionapp.web.rest.TestUtil;

public class UserMissionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserMissionDTO.class);
        UserMissionDTO userMissionDTO1 = new UserMissionDTO();
        userMissionDTO1.setId(1L);
        UserMissionDTO userMissionDTO2 = new UserMissionDTO();
        assertThat(userMissionDTO1).isNotEqualTo(userMissionDTO2);
        userMissionDTO2.setId(userMissionDTO1.getId());
        assertThat(userMissionDTO1).isEqualTo(userMissionDTO2);
        userMissionDTO2.setId(2L);
        assertThat(userMissionDTO1).isNotEqualTo(userMissionDTO2);
        userMissionDTO1.setId(null);
        assertThat(userMissionDTO1).isNotEqualTo(userMissionDTO2);
    }
}
