package com.prather.missionapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import com.prather.missionapp.domain.dto.UserCityDTO;
import com.prather.missionapp.web.rest.TestUtil;

public class UserCityDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserCityDTO.class);
        UserCityDTO userCityDTO1 = new UserCityDTO();
        userCityDTO1.setId(1L);
        UserCityDTO userCityDTO2 = new UserCityDTO();
        assertThat(userCityDTO1).isNotEqualTo(userCityDTO2);
        userCityDTO2.setId(userCityDTO1.getId());
        assertThat(userCityDTO1).isEqualTo(userCityDTO2);
        userCityDTO2.setId(2L);
        assertThat(userCityDTO1).isNotEqualTo(userCityDTO2);
        userCityDTO1.setId(null);
        assertThat(userCityDTO1).isNotEqualTo(userCityDTO2);
    }
}
