package com.prather.missionapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.prather.missionapp.web.rest.TestUtil;

public class UserCityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserCity.class);
        UserCity userCity1 = new UserCity();
        userCity1.setId(1L);
        UserCity userCity2 = new UserCity();
        userCity2.setId(userCity1.getId());
        assertThat(userCity1).isEqualTo(userCity2);
        userCity2.setId(2L);
        assertThat(userCity1).isNotEqualTo(userCity2);
        userCity1.setId(null);
        assertThat(userCity1).isNotEqualTo(userCity2);
    }
}
