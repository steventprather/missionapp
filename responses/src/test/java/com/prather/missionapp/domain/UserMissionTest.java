package com.prather.missionapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.prather.missionapp.web.rest.TestUtil;

public class UserMissionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserMission.class);
        UserMission userMission1 = new UserMission();
        userMission1.setId(1L);
        UserMission userMission2 = new UserMission();
        userMission2.setId(userMission1.getId());
        assertThat(userMission1).isEqualTo(userMission2);
        userMission2.setId(2L);
        assertThat(userMission1).isNotEqualTo(userMission2);
        userMission1.setId(null);
        assertThat(userMission1).isNotEqualTo(userMission2);
    }
}
