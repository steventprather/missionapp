This is an idea that came to me a few years ago when we took my 6 year old son to Rome. We purchased this book:
https://www.amazon.com/Catherine-Aragon/dp/098922676X/ref=sr_1_2?dchild=1&keywords=mission+rome&qid=1594833165&sr=8-2

to turn the trip into a spy game, which kept him engaged long enough for us to check out the buildings, artwork, etc. It worked really well. We ran into another family with the book at the Roman Forum. They loved it as well. Multiple people stopped us to ask about it. If there was an app version, I think they would have bought it then.

I have finally decided to build it!

I plan to use the jhipster project microservices capabilities for a couple of reasons. First, I don't want to spend a lot of time filling in boilerplate. Jhipster has generators to generate the backend model and generic frontend display/edit pages. They have simple authentication and user admin already built in. I can forego building that and get straight to the fun part!

Do I need to build microservices? No, it won't be that big of an application. Am I going to? Yes, for kicks.

I want the front-end to be a Progressive Web App, so the user can save it to their homescreen for easy access.

There are two user sets
1. Admin - people who can create/update missions
2. Users - people would subscribe to cities/landmarks. They would be given a list of "tasks" to complete and tally up points along the way. I assume the user would want to take pictures at each of these landmarks

I have a rough idea of how I want this to work
Set up application gateway to handle user authentication and relaying requests to microservices
Set up missions service to manage the definitions of the cities/missions/objectives (ie, in London, we create missions to fulfill at Buckingham palace)
Set up a subscriptions/responses service for user to enter the missions they complete and to tally up points
Set up an account service to handle payments. I think we could set it up so the user could either purchase everything in a city or just certain landmarks. Either way, that is a long way off.

To run locally
1. Run consul locally in a docker contiainer according to: https://www.jhipster.tech/consul/
2. Run postgresql in a docker container and create a database. If you want to install postgresql directly, or use a server you have handy, then just update the application-dev.yml file appropriately.
2. Run the gateway service
3. Run other services as they are added

Questions to answer
1. v1 will not store data locally. It will just be a PWA that manages everything on the server. Should I bother to store the data locally? It would be nice to be able to use the data without being connected. But, this is becoming less of a requirement as time goes on. I'm going to punt this until later in an effort to get something out.
2. How will I get people to design scavenger hunts for cities that I don't know much about? That also can be answered later.
3. In the future, could we set up a generic book using Shutterfly or something like that, and fill in the user's pictures to create a custom vacation book for them.
