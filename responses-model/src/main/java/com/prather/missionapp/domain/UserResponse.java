package com.prather.missionapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A UserResponse.
 */
@Entity
@Table(name = "user_response")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "objective_id")
    private Long objectiveId;

    @Column(name = "checked")
    private Boolean checked;

    @Column(name = "items")
    private Integer items;

    @Column(name = "points")
    private Integer points;

    @Column(name = "text")
    private String text;

    @ManyToOne
    @JsonIgnoreProperties(value = "userResponses", allowSetters = true)
    private UserMission userMission;

    @ManyToOne
    @JsonIgnoreProperties(value = "userResponses", allowSetters = true)
    private UserResponse parent;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getObjectiveId() {
        return objectiveId;
    }

    public UserResponse objectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
        return this;
    }

    public void setObjectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
    }

    public Boolean isChecked() {
        return checked;
    }

    public UserResponse checked(Boolean checked) {
        this.checked = checked;
        return this;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Integer getItems() {
        return items;
    }

    public UserResponse items(Integer items) {
        this.items = items;
        return this;
    }

    public void setItems(Integer items) {
        this.items = items;
    }

    public Integer getPoints() {
        return points;
    }

    public UserResponse points(Integer points) {
        this.points = points;
        return this;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getText() {
        return text;
    }

    public UserResponse text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UserMission getUserMission() {
        return userMission;
    }

    public UserResponse userMission(UserMission userMission) {
        this.userMission = userMission;
        return this;
    }

    public void setUserMission(UserMission userMission) {
        this.userMission = userMission;
    }

    public UserResponse getParent() {
        return parent;
    }

    public UserResponse parent(UserResponse userResponse) {
        this.parent = userResponse;
        return this;
    }

    public void setParent(UserResponse userResponse) {
        this.parent = userResponse;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserResponse)) {
            return false;
        }
        return id != null && id.equals(((UserResponse) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserResponse{" +
            "id=" + getId() +
            ", objectiveId=" + getObjectiveId() +
            ", checked='" + isChecked() + "'" +
            ", items=" + getItems() +
            ", points=" + getPoints() +
            ", text='" + getText() + "'" +
            "}";
    }
}
