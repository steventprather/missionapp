package com.prather.missionapp.domain.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.prather.missionapp.domain.UserMission} entity.
 */
public class UserMissionDTO implements Serializable {
    
    private Long id;

    private String userId;

    private Long missionId;


    private Long userCityId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getMissionId() {
        return missionId;
    }

    public void setMissionId(Long missionId) {
        this.missionId = missionId;
    }

    public Long getUserCityId() {
        return userCityId;
    }

    public void setUserCityId(Long userCityId) {
        this.userCityId = userCityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserMissionDTO)) {
            return false;
        }

        return id != null && id.equals(((UserMissionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserMissionDTO{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", missionId=" + getMissionId() +
            ", userCityId=" + getUserCityId() +
            "}";
    }
}
