package com.prather.missionapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A UserMission.
 */
@Entity
@Table(name = "user_mission")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserMission implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "mission_id")
    private Long missionId;

    @ManyToOne
    @JsonIgnoreProperties(value = "userMissions", allowSetters = true)
    private UserCity userCity;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public UserMission userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getMissionId() {
        return missionId;
    }

    public UserMission missionId(Long missionId) {
        this.missionId = missionId;
        return this;
    }

    public void setMissionId(Long missionId) {
        this.missionId = missionId;
    }

    public UserCity getUserCity() {
        return userCity;
    }

    public UserMission userCity(UserCity userCity) {
        this.userCity = userCity;
        return this;
    }

    public void setUserCity(UserCity userCity) {
        this.userCity = userCity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserMission)) {
            return false;
        }
        return id != null && id.equals(((UserMission) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserMission{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", missionId=" + getMissionId() +
            "}";
    }
}
