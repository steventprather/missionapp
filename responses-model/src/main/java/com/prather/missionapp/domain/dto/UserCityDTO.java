package com.prather.missionapp.domain.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.prather.missionapp.domain.UserCity} entity.
 */
public class UserCityDTO implements Serializable {
    
    private Long id;

    private String userId;

    private Long cityId;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserCityDTO)) {
            return false;
        }

        return id != null && id.equals(((UserCityDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserCityDTO{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", cityId=" + getCityId() +
            "}";
    }
}
