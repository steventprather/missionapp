package com.prather.missionapp.domain.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.prather.missionapp.domain.UserResponse} entity.
 */
public class UserResponseDTO implements Serializable {
    
    private Long id;

    private Long objectiveId;

    private Boolean checked;

    private Integer items;

    private Integer points;

    private String text;


    private Long userMissionId;

    private Long parentId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getObjectiveId() {
        return objectiveId;
    }

    public void setObjectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
    }

    public Boolean isChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Integer getItems() {
        return items;
    }

    public void setItems(Integer items) {
        this.items = items;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getUserMissionId() {
        return userMissionId;
    }

    public void setUserMissionId(Long userMissionId) {
        this.userMissionId = userMissionId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long userResponseId) {
        this.parentId = userResponseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserResponseDTO)) {
            return false;
        }

        return id != null && id.equals(((UserResponseDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserResponseDTO{" +
            "id=" + getId() +
            ", objectiveId=" + getObjectiveId() +
            ", checked='" + isChecked() + "'" +
            ", items=" + getItems() +
            ", points=" + getPoints() +
            ", text='" + getText() + "'" +
            ", userMissionId=" + getUserMissionId() +
            ", parentId=" + getParentId() +
            "}";
    }
}
