package com.prather.missionapp.gateway.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prather.missionapp.domain.dto.UserDTO;


@FeignClient(name = "gateway/api/users")
public interface UserClient {

	@RequestMapping(value = "/{login}", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> getUser(@PathVariable("login") String login);

}
