package com.prather.missionapp.web.rest;

import com.prather.missionapp.MissionsApp;
import com.prather.missionapp.domain.CityImage;
import com.prather.missionapp.domain.dto.CityImageDTO;
import com.prather.missionapp.repository.CityImageRepository;
import com.prather.missionapp.service.CityImageService;
import com.prather.missionapp.service.mapper.CityImageMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CityImageResource} REST controller.
 */
@SpringBootTest(classes = MissionsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CityImageResourceIT {

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    @Autowired
    private CityImageRepository cityImageRepository;

    @Autowired
    private CityImageMapper cityImageMapper;

    @Autowired
    private CityImageService cityImageService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCityImageMockMvc;

    private CityImage cityImage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CityImage createEntity(EntityManager em) {
        CityImage cityImage = new CityImage()
            .fileName(DEFAULT_FILE_NAME);
        return cityImage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CityImage createUpdatedEntity(EntityManager em) {
        CityImage cityImage = new CityImage()
            .fileName(UPDATED_FILE_NAME);
        return cityImage;
    }

    @BeforeEach
    public void initTest() {
        cityImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createCityImage() throws Exception {
        int databaseSizeBeforeCreate = cityImageRepository.findAll().size();
        // Create the CityImage
        CityImageDTO cityImageDTO = cityImageMapper.toDto(cityImage);
        restCityImageMockMvc.perform(post("/api/city-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityImageDTO)))
            .andExpect(status().isCreated());

        // Validate the CityImage in the database
        List<CityImage> cityImageList = cityImageRepository.findAll();
        assertThat(cityImageList).hasSize(databaseSizeBeforeCreate + 1);
        CityImage testCityImage = cityImageList.get(cityImageList.size() - 1);
        assertThat(testCityImage.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
    }

    @Test
    @Transactional
    public void createCityImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cityImageRepository.findAll().size();

        // Create the CityImage with an existing ID
        cityImage.setId(1L);
        CityImageDTO cityImageDTO = cityImageMapper.toDto(cityImage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCityImageMockMvc.perform(post("/api/city-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityImageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CityImage in the database
        List<CityImage> cityImageList = cityImageRepository.findAll();
        assertThat(cityImageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCityImages() throws Exception {
        // Initialize the database
        cityImageRepository.saveAndFlush(cityImage);

        // Get all the cityImageList
        restCityImageMockMvc.perform(get("/api/city-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cityImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)));
    }
    
    @Test
    @Transactional
    public void getCityImage() throws Exception {
        // Initialize the database
        cityImageRepository.saveAndFlush(cityImage);

        // Get the cityImage
        restCityImageMockMvc.perform(get("/api/city-images/{id}", cityImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cityImage.getId().intValue()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingCityImage() throws Exception {
        // Get the cityImage
        restCityImageMockMvc.perform(get("/api/city-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCityImage() throws Exception {
        // Initialize the database
        cityImageRepository.saveAndFlush(cityImage);

        int databaseSizeBeforeUpdate = cityImageRepository.findAll().size();

        // Update the cityImage
        CityImage updatedCityImage = cityImageRepository.findById(cityImage.getId()).get();
        // Disconnect from session so that the updates on updatedCityImage are not directly saved in db
        em.detach(updatedCityImage);
        updatedCityImage
            .fileName(UPDATED_FILE_NAME);
        CityImageDTO cityImageDTO = cityImageMapper.toDto(updatedCityImage);

        restCityImageMockMvc.perform(put("/api/city-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityImageDTO)))
            .andExpect(status().isOk());

        // Validate the CityImage in the database
        List<CityImage> cityImageList = cityImageRepository.findAll();
        assertThat(cityImageList).hasSize(databaseSizeBeforeUpdate);
        CityImage testCityImage = cityImageList.get(cityImageList.size() - 1);
        assertThat(testCityImage.getFileName()).isEqualTo(UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingCityImage() throws Exception {
        int databaseSizeBeforeUpdate = cityImageRepository.findAll().size();

        // Create the CityImage
        CityImageDTO cityImageDTO = cityImageMapper.toDto(cityImage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCityImageMockMvc.perform(put("/api/city-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityImageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CityImage in the database
        List<CityImage> cityImageList = cityImageRepository.findAll();
        assertThat(cityImageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCityImage() throws Exception {
        // Initialize the database
        cityImageRepository.saveAndFlush(cityImage);

        int databaseSizeBeforeDelete = cityImageRepository.findAll().size();

        // Delete the cityImage
        restCityImageMockMvc.perform(delete("/api/city-images/{id}", cityImage.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CityImage> cityImageList = cityImageRepository.findAll();
        assertThat(cityImageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
