package com.prather.missionapp.web.rest;

import com.prather.missionapp.MissionsApp;
import com.prather.missionapp.domain.MissionImage;
import com.prather.missionapp.domain.dto.MissionImageDTO;
import com.prather.missionapp.repository.MissionImageRepository;
import com.prather.missionapp.service.MissionImageService;
import com.prather.missionapp.service.mapper.MissionImageMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MissionImageResource} REST controller.
 */
@SpringBootTest(classes = MissionsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MissionImageResourceIT {

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    @Autowired
    private MissionImageRepository missionImageRepository;

    @Autowired
    private MissionImageMapper missionImageMapper;

    @Autowired
    private MissionImageService missionImageService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMissionImageMockMvc;

    private MissionImage missionImage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MissionImage createEntity(EntityManager em) {
        MissionImage missionImage = new MissionImage()
            .fileName(DEFAULT_FILE_NAME);
        return missionImage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MissionImage createUpdatedEntity(EntityManager em) {
        MissionImage missionImage = new MissionImage()
            .fileName(UPDATED_FILE_NAME);
        return missionImage;
    }

    @BeforeEach
    public void initTest() {
        missionImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createMissionImage() throws Exception {
        int databaseSizeBeforeCreate = missionImageRepository.findAll().size();
        // Create the MissionImage
        MissionImageDTO missionImageDTO = missionImageMapper.toDto(missionImage);
        restMissionImageMockMvc.perform(post("/api/mission-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(missionImageDTO)))
            .andExpect(status().isCreated());

        // Validate the MissionImage in the database
        List<MissionImage> missionImageList = missionImageRepository.findAll();
        assertThat(missionImageList).hasSize(databaseSizeBeforeCreate + 1);
        MissionImage testMissionImage = missionImageList.get(missionImageList.size() - 1);
        assertThat(testMissionImage.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
    }

    @Test
    @Transactional
    public void createMissionImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = missionImageRepository.findAll().size();

        // Create the MissionImage with an existing ID
        missionImage.setId(1L);
        MissionImageDTO missionImageDTO = missionImageMapper.toDto(missionImage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMissionImageMockMvc.perform(post("/api/mission-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(missionImageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MissionImage in the database
        List<MissionImage> missionImageList = missionImageRepository.findAll();
        assertThat(missionImageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMissionImages() throws Exception {
        // Initialize the database
        missionImageRepository.saveAndFlush(missionImage);

        // Get all the missionImageList
        restMissionImageMockMvc.perform(get("/api/mission-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(missionImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)));
    }
    
    @Test
    @Transactional
    public void getMissionImage() throws Exception {
        // Initialize the database
        missionImageRepository.saveAndFlush(missionImage);

        // Get the missionImage
        restMissionImageMockMvc.perform(get("/api/mission-images/{id}", missionImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(missionImage.getId().intValue()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingMissionImage() throws Exception {
        // Get the missionImage
        restMissionImageMockMvc.perform(get("/api/mission-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMissionImage() throws Exception {
        // Initialize the database
        missionImageRepository.saveAndFlush(missionImage);

        int databaseSizeBeforeUpdate = missionImageRepository.findAll().size();

        // Update the missionImage
        MissionImage updatedMissionImage = missionImageRepository.findById(missionImage.getId()).get();
        // Disconnect from session so that the updates on updatedMissionImage are not directly saved in db
        em.detach(updatedMissionImage);
        updatedMissionImage
            .fileName(UPDATED_FILE_NAME);
        MissionImageDTO missionImageDTO = missionImageMapper.toDto(updatedMissionImage);

        restMissionImageMockMvc.perform(put("/api/mission-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(missionImageDTO)))
            .andExpect(status().isOk());

        // Validate the MissionImage in the database
        List<MissionImage> missionImageList = missionImageRepository.findAll();
        assertThat(missionImageList).hasSize(databaseSizeBeforeUpdate);
        MissionImage testMissionImage = missionImageList.get(missionImageList.size() - 1);
        assertThat(testMissionImage.getFileName()).isEqualTo(UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingMissionImage() throws Exception {
        int databaseSizeBeforeUpdate = missionImageRepository.findAll().size();

        // Create the MissionImage
        MissionImageDTO missionImageDTO = missionImageMapper.toDto(missionImage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMissionImageMockMvc.perform(put("/api/mission-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(missionImageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MissionImage in the database
        List<MissionImage> missionImageList = missionImageRepository.findAll();
        assertThat(missionImageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMissionImage() throws Exception {
        // Initialize the database
        missionImageRepository.saveAndFlush(missionImage);

        int databaseSizeBeforeDelete = missionImageRepository.findAll().size();

        // Delete the missionImage
        restMissionImageMockMvc.perform(delete("/api/mission-images/{id}", missionImage.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MissionImage> missionImageList = missionImageRepository.findAll();
        assertThat(missionImageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
