package com.prather.missionapp.web.rest;

import com.prather.missionapp.MissionsApp;
import com.prather.missionapp.domain.ObjectiveImage;
import com.prather.missionapp.domain.dto.ObjectiveImageDTO;
import com.prather.missionapp.repository.ObjectiveImageRepository;
import com.prather.missionapp.service.ObjectiveImageService;
import com.prather.missionapp.service.mapper.ObjectiveImageMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ObjectiveImageResource} REST controller.
 */
@SpringBootTest(classes = MissionsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ObjectiveImageResourceIT {

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    @Autowired
    private ObjectiveImageRepository objectiveImageRepository;

    @Autowired
    private ObjectiveImageMapper objectiveImageMapper;

    @Autowired
    private ObjectiveImageService objectiveImageService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restObjectiveImageMockMvc;

    private ObjectiveImage objectiveImage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ObjectiveImage createEntity(EntityManager em) {
        ObjectiveImage objectiveImage = new ObjectiveImage()
            .fileName(DEFAULT_FILE_NAME);
        return objectiveImage;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ObjectiveImage createUpdatedEntity(EntityManager em) {
        ObjectiveImage objectiveImage = new ObjectiveImage()
            .fileName(UPDATED_FILE_NAME);
        return objectiveImage;
    }

    @BeforeEach
    public void initTest() {
        objectiveImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createObjectiveImage() throws Exception {
        int databaseSizeBeforeCreate = objectiveImageRepository.findAll().size();
        // Create the ObjectiveImage
        ObjectiveImageDTO objectiveImageDTO = objectiveImageMapper.toDto(objectiveImage);
        restObjectiveImageMockMvc.perform(post("/api/objective-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(objectiveImageDTO)))
            .andExpect(status().isCreated());

        // Validate the ObjectiveImage in the database
        List<ObjectiveImage> objectiveImageList = objectiveImageRepository.findAll();
        assertThat(objectiveImageList).hasSize(databaseSizeBeforeCreate + 1);
        ObjectiveImage testObjectiveImage = objectiveImageList.get(objectiveImageList.size() - 1);
        assertThat(testObjectiveImage.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
    }

    @Test
    @Transactional
    public void createObjectiveImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = objectiveImageRepository.findAll().size();

        // Create the ObjectiveImage with an existing ID
        objectiveImage.setId(1L);
        ObjectiveImageDTO objectiveImageDTO = objectiveImageMapper.toDto(objectiveImage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restObjectiveImageMockMvc.perform(post("/api/objective-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(objectiveImageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ObjectiveImage in the database
        List<ObjectiveImage> objectiveImageList = objectiveImageRepository.findAll();
        assertThat(objectiveImageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllObjectiveImages() throws Exception {
        // Initialize the database
        objectiveImageRepository.saveAndFlush(objectiveImage);

        // Get all the objectiveImageList
        restObjectiveImageMockMvc.perform(get("/api/objective-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(objectiveImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)));
    }
    
    @Test
    @Transactional
    public void getObjectiveImage() throws Exception {
        // Initialize the database
        objectiveImageRepository.saveAndFlush(objectiveImage);

        // Get the objectiveImage
        restObjectiveImageMockMvc.perform(get("/api/objective-images/{id}", objectiveImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(objectiveImage.getId().intValue()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingObjectiveImage() throws Exception {
        // Get the objectiveImage
        restObjectiveImageMockMvc.perform(get("/api/objective-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateObjectiveImage() throws Exception {
        // Initialize the database
        objectiveImageRepository.saveAndFlush(objectiveImage);

        int databaseSizeBeforeUpdate = objectiveImageRepository.findAll().size();

        // Update the objectiveImage
        ObjectiveImage updatedObjectiveImage = objectiveImageRepository.findById(objectiveImage.getId()).get();
        // Disconnect from session so that the updates on updatedObjectiveImage are not directly saved in db
        em.detach(updatedObjectiveImage);
        updatedObjectiveImage
            .fileName(UPDATED_FILE_NAME);
        ObjectiveImageDTO objectiveImageDTO = objectiveImageMapper.toDto(updatedObjectiveImage);

        restObjectiveImageMockMvc.perform(put("/api/objective-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(objectiveImageDTO)))
            .andExpect(status().isOk());

        // Validate the ObjectiveImage in the database
        List<ObjectiveImage> objectiveImageList = objectiveImageRepository.findAll();
        assertThat(objectiveImageList).hasSize(databaseSizeBeforeUpdate);
        ObjectiveImage testObjectiveImage = objectiveImageList.get(objectiveImageList.size() - 1);
        assertThat(testObjectiveImage.getFileName()).isEqualTo(UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingObjectiveImage() throws Exception {
        int databaseSizeBeforeUpdate = objectiveImageRepository.findAll().size();

        // Create the ObjectiveImage
        ObjectiveImageDTO objectiveImageDTO = objectiveImageMapper.toDto(objectiveImage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restObjectiveImageMockMvc.perform(put("/api/objective-images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(objectiveImageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ObjectiveImage in the database
        List<ObjectiveImage> objectiveImageList = objectiveImageRepository.findAll();
        assertThat(objectiveImageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteObjectiveImage() throws Exception {
        // Initialize the database
        objectiveImageRepository.saveAndFlush(objectiveImage);

        int databaseSizeBeforeDelete = objectiveImageRepository.findAll().size();

        // Delete the objectiveImage
        restObjectiveImageMockMvc.perform(delete("/api/objective-images/{id}", objectiveImage.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ObjectiveImage> objectiveImageList = objectiveImageRepository.findAll();
        assertThat(objectiveImageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
