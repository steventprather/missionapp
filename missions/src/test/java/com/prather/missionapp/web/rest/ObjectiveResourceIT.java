package com.prather.missionapp.web.rest;

import com.prather.missionapp.MissionsApp;
import com.prather.missionapp.domain.Objective;
import com.prather.missionapp.domain.dto.ObjectiveDTO;
import com.prather.missionapp.repository.ObjectiveRepository;
import com.prather.missionapp.service.ObjectiveService;
import com.prather.missionapp.service.mapper.ObjectiveMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.prather.missionapp.domain.enumeration.ObjectiveType;
/**
 * Integration tests for the {@link ObjectiveResource} REST controller.
 */
@SpringBootTest(classes = MissionsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ObjectiveResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final ObjectiveType DEFAULT_TYPE = ObjectiveType.PARENT;
    private static final ObjectiveType UPDATED_TYPE = ObjectiveType.PERITEM;

    private static final Integer DEFAULT_ORDER = 1;
    private static final Integer UPDATED_ORDER = 2;

    private static final Integer DEFAULT_MAX_POINTS = 1;
    private static final Integer UPDATED_MAX_POINTS = 2;

    private static final Integer DEFAULT_POINTS_PER_ITEM = 1;
    private static final Integer UPDATED_POINTS_PER_ITEM = 2;

    private static final Integer DEFAULT_POINTS = 1;
    private static final Integer UPDATED_POINTS = 2;

    private static final String DEFAULT_IMAGE_BASE = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_BASE = "BBBBBBBBBB";

    @Autowired
    private ObjectiveRepository objectiveRepository;

    @Autowired
    private ObjectiveMapper objectiveMapper;

    @Autowired
    private ObjectiveService objectiveService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restObjectiveMockMvc;

    private Objective objective;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Objective createEntity(EntityManager em) {
        Objective objective = new Objective()
            .name(DEFAULT_NAME)
            .text(DEFAULT_TEXT)
            .type(DEFAULT_TYPE)
            .order(DEFAULT_ORDER)
            .maxPoints(DEFAULT_MAX_POINTS)
            .pointsPerItem(DEFAULT_POINTS_PER_ITEM)
            .points(DEFAULT_POINTS)
            .imageBase(DEFAULT_IMAGE_BASE);
        return objective;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Objective createUpdatedEntity(EntityManager em) {
        Objective objective = new Objective()
            .name(UPDATED_NAME)
            .text(UPDATED_TEXT)
            .type(UPDATED_TYPE)
            .order(UPDATED_ORDER)
            .maxPoints(UPDATED_MAX_POINTS)
            .pointsPerItem(UPDATED_POINTS_PER_ITEM)
            .points(UPDATED_POINTS)
            .imageBase(UPDATED_IMAGE_BASE);
        return objective;
    }

    @BeforeEach
    public void initTest() {
        objective = createEntity(em);
    }

    @Test
    @Transactional
    public void createObjective() throws Exception {
        int databaseSizeBeforeCreate = objectiveRepository.findAll().size();
        // Create the Objective
        ObjectiveDTO objectiveDTO = objectiveMapper.toDto(objective);
        restObjectiveMockMvc.perform(post("/api/objectives")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(objectiveDTO)))
            .andExpect(status().isCreated());

        // Validate the Objective in the database
        List<Objective> objectiveList = objectiveRepository.findAll();
        assertThat(objectiveList).hasSize(databaseSizeBeforeCreate + 1);
        Objective testObjective = objectiveList.get(objectiveList.size() - 1);
        assertThat(testObjective.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testObjective.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testObjective.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testObjective.getOrder()).isEqualTo(DEFAULT_ORDER);
        assertThat(testObjective.getMaxPoints()).isEqualTo(DEFAULT_MAX_POINTS);
        assertThat(testObjective.getPointsPerItem()).isEqualTo(DEFAULT_POINTS_PER_ITEM);
        assertThat(testObjective.getPoints()).isEqualTo(DEFAULT_POINTS);
        assertThat(testObjective.getImageBase()).isEqualTo(DEFAULT_IMAGE_BASE);
    }

    @Test
    @Transactional
    public void createObjectiveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = objectiveRepository.findAll().size();

        // Create the Objective with an existing ID
        objective.setId(1L);
        ObjectiveDTO objectiveDTO = objectiveMapper.toDto(objective);

        // An entity with an existing ID cannot be created, so this API call must fail
        restObjectiveMockMvc.perform(post("/api/objectives")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(objectiveDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Objective in the database
        List<Objective> objectiveList = objectiveRepository.findAll();
        assertThat(objectiveList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllObjectives() throws Exception {
        // Initialize the database
        objectiveRepository.saveAndFlush(objective);

        // Get all the objectiveList
        restObjectiveMockMvc.perform(get("/api/objectives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(objective.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)))
            .andExpect(jsonPath("$.[*].maxPoints").value(hasItem(DEFAULT_MAX_POINTS)))
            .andExpect(jsonPath("$.[*].pointsPerItem").value(hasItem(DEFAULT_POINTS_PER_ITEM)))
            .andExpect(jsonPath("$.[*].points").value(hasItem(DEFAULT_POINTS)))
            .andExpect(jsonPath("$.[*].imageBase").value(hasItem(DEFAULT_IMAGE_BASE)));
    }
    
    @Test
    @Transactional
    public void getObjective() throws Exception {
        // Initialize the database
        objectiveRepository.saveAndFlush(objective);

        // Get the objective
        restObjectiveMockMvc.perform(get("/api/objectives/{id}", objective.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(objective.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER))
            .andExpect(jsonPath("$.maxPoints").value(DEFAULT_MAX_POINTS))
            .andExpect(jsonPath("$.pointsPerItem").value(DEFAULT_POINTS_PER_ITEM))
            .andExpect(jsonPath("$.points").value(DEFAULT_POINTS))
            .andExpect(jsonPath("$.imageBase").value(DEFAULT_IMAGE_BASE));
    }
    @Test
    @Transactional
    public void getNonExistingObjective() throws Exception {
        // Get the objective
        restObjectiveMockMvc.perform(get("/api/objectives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateObjective() throws Exception {
        // Initialize the database
        objectiveRepository.saveAndFlush(objective);

        int databaseSizeBeforeUpdate = objectiveRepository.findAll().size();

        // Update the objective
        Objective updatedObjective = objectiveRepository.findById(objective.getId()).get();
        // Disconnect from session so that the updates on updatedObjective are not directly saved in db
        em.detach(updatedObjective);
        updatedObjective
            .name(UPDATED_NAME)
            .text(UPDATED_TEXT)
            .type(UPDATED_TYPE)
            .order(UPDATED_ORDER)
            .maxPoints(UPDATED_MAX_POINTS)
            .pointsPerItem(UPDATED_POINTS_PER_ITEM)
            .points(UPDATED_POINTS)
            .imageBase(UPDATED_IMAGE_BASE);
        ObjectiveDTO objectiveDTO = objectiveMapper.toDto(updatedObjective);

        restObjectiveMockMvc.perform(put("/api/objectives")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(objectiveDTO)))
            .andExpect(status().isOk());

        // Validate the Objective in the database
        List<Objective> objectiveList = objectiveRepository.findAll();
        assertThat(objectiveList).hasSize(databaseSizeBeforeUpdate);
        Objective testObjective = objectiveList.get(objectiveList.size() - 1);
        assertThat(testObjective.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testObjective.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testObjective.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testObjective.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testObjective.getMaxPoints()).isEqualTo(UPDATED_MAX_POINTS);
        assertThat(testObjective.getPointsPerItem()).isEqualTo(UPDATED_POINTS_PER_ITEM);
        assertThat(testObjective.getPoints()).isEqualTo(UPDATED_POINTS);
        assertThat(testObjective.getImageBase()).isEqualTo(UPDATED_IMAGE_BASE);
    }

    @Test
    @Transactional
    public void updateNonExistingObjective() throws Exception {
        int databaseSizeBeforeUpdate = objectiveRepository.findAll().size();

        // Create the Objective
        ObjectiveDTO objectiveDTO = objectiveMapper.toDto(objective);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restObjectiveMockMvc.perform(put("/api/objectives")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(objectiveDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Objective in the database
        List<Objective> objectiveList = objectiveRepository.findAll();
        assertThat(objectiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteObjective() throws Exception {
        // Initialize the database
        objectiveRepository.saveAndFlush(objective);

        int databaseSizeBeforeDelete = objectiveRepository.findAll().size();

        // Delete the objective
        restObjectiveMockMvc.perform(delete("/api/objectives/{id}", objective.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Objective> objectiveList = objectiveRepository.findAll();
        assertThat(objectiveList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
