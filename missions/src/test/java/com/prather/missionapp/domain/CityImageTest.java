package com.prather.missionapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.prather.missionapp.web.rest.TestUtil;

public class CityImageTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CityImage.class);
        CityImage cityImage1 = new CityImage();
        cityImage1.setId(1L);
        CityImage cityImage2 = new CityImage();
        cityImage2.setId(cityImage1.getId());
        assertThat(cityImage1).isEqualTo(cityImage2);
        cityImage2.setId(2L);
        assertThat(cityImage1).isNotEqualTo(cityImage2);
        cityImage1.setId(null);
        assertThat(cityImage1).isNotEqualTo(cityImage2);
    }
}
