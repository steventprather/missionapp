package com.prather.missionapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.prather.missionapp.web.rest.TestUtil;

public class ObjectiveImageTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ObjectiveImage.class);
        ObjectiveImage objectiveImage1 = new ObjectiveImage();
        objectiveImage1.setId(1L);
        ObjectiveImage objectiveImage2 = new ObjectiveImage();
        objectiveImage2.setId(objectiveImage1.getId());
        assertThat(objectiveImage1).isEqualTo(objectiveImage2);
        objectiveImage2.setId(2L);
        assertThat(objectiveImage1).isNotEqualTo(objectiveImage2);
        objectiveImage1.setId(null);
        assertThat(objectiveImage1).isNotEqualTo(objectiveImage2);
    }
}
