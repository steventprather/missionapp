package com.prather.missionapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.prather.missionapp.web.rest.TestUtil;

public class MissionImageTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MissionImage.class);
        MissionImage missionImage1 = new MissionImage();
        missionImage1.setId(1L);
        MissionImage missionImage2 = new MissionImage();
        missionImage2.setId(missionImage1.getId());
        assertThat(missionImage1).isEqualTo(missionImage2);
        missionImage2.setId(2L);
        assertThat(missionImage1).isNotEqualTo(missionImage2);
        missionImage1.setId(null);
        assertThat(missionImage1).isNotEqualTo(missionImage2);
    }
}
