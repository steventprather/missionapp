package com.prather.missionapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import com.prather.missionapp.domain.dto.ObjectiveImageDTO;
import com.prather.missionapp.web.rest.TestUtil;

public class ObjectiveImageDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ObjectiveImageDTO.class);
        ObjectiveImageDTO objectiveImageDTO1 = new ObjectiveImageDTO();
        objectiveImageDTO1.setId(1L);
        ObjectiveImageDTO objectiveImageDTO2 = new ObjectiveImageDTO();
        assertThat(objectiveImageDTO1).isNotEqualTo(objectiveImageDTO2);
        objectiveImageDTO2.setId(objectiveImageDTO1.getId());
        assertThat(objectiveImageDTO1).isEqualTo(objectiveImageDTO2);
        objectiveImageDTO2.setId(2L);
        assertThat(objectiveImageDTO1).isNotEqualTo(objectiveImageDTO2);
        objectiveImageDTO1.setId(null);
        assertThat(objectiveImageDTO1).isNotEqualTo(objectiveImageDTO2);
    }
}
