package com.prather.missionapp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CityImageMapperTest {

    private CityImageMapper cityImageMapper;

    @BeforeEach
    public void setUp() {
        cityImageMapper = new CityImageMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(cityImageMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(cityImageMapper.fromId(null)).isNull();
    }
}
