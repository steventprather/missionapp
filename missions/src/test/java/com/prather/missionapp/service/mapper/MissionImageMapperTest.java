package com.prather.missionapp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class MissionImageMapperTest {

    private MissionImageMapper missionImageMapper;

    @BeforeEach
    public void setUp() {
        missionImageMapper = new MissionImageMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(missionImageMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(missionImageMapper.fromId(null)).isNull();
    }
}
