package com.prather.missionapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import com.prather.missionapp.domain.dto.MissionImageDTO;
import com.prather.missionapp.web.rest.TestUtil;

public class MissionImageDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MissionImageDTO.class);
        MissionImageDTO missionImageDTO1 = new MissionImageDTO();
        missionImageDTO1.setId(1L);
        MissionImageDTO missionImageDTO2 = new MissionImageDTO();
        assertThat(missionImageDTO1).isNotEqualTo(missionImageDTO2);
        missionImageDTO2.setId(missionImageDTO1.getId());
        assertThat(missionImageDTO1).isEqualTo(missionImageDTO2);
        missionImageDTO2.setId(2L);
        assertThat(missionImageDTO1).isNotEqualTo(missionImageDTO2);
        missionImageDTO1.setId(null);
        assertThat(missionImageDTO1).isNotEqualTo(missionImageDTO2);
    }
}
