package com.prather.missionapp.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ObjectiveImageMapperTest {

    private ObjectiveImageMapper objectiveImageMapper;

    @BeforeEach
    public void setUp() {
        objectiveImageMapper = new ObjectiveImageMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(objectiveImageMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(objectiveImageMapper.fromId(null)).isNull();
    }
}
