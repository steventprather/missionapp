package com.prather.missionapp.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import com.prather.missionapp.domain.dto.CityImageDTO;
import com.prather.missionapp.web.rest.TestUtil;

public class CityImageDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CityImageDTO.class);
        CityImageDTO cityImageDTO1 = new CityImageDTO();
        cityImageDTO1.setId(1L);
        CityImageDTO cityImageDTO2 = new CityImageDTO();
        assertThat(cityImageDTO1).isNotEqualTo(cityImageDTO2);
        cityImageDTO2.setId(cityImageDTO1.getId());
        assertThat(cityImageDTO1).isEqualTo(cityImageDTO2);
        cityImageDTO2.setId(2L);
        assertThat(cityImageDTO1).isNotEqualTo(cityImageDTO2);
        cityImageDTO1.setId(null);
        assertThat(cityImageDTO1).isNotEqualTo(cityImageDTO2);
    }
}
