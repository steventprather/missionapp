package com.prather.missionapp.repository;

import com.prather.missionapp.domain.ObjectiveImage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ObjectiveImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ObjectiveImageRepository extends JpaRepository<ObjectiveImage, Long> {
}
