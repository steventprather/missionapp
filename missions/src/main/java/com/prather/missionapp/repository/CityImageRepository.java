package com.prather.missionapp.repository;

import com.prather.missionapp.domain.CityImage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CityImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityImageRepository extends JpaRepository<CityImage, Long> {
}
