package com.prather.missionapp.repository;

import com.prather.missionapp.domain.MissionImage;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the MissionImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MissionImageRepository extends JpaRepository<MissionImage, Long> {
}
