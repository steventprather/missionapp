package com.prather.missionapp.service.mapper;


import com.prather.missionapp.domain.*;
import com.prather.missionapp.domain.dto.MissionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Mission} and its DTO {@link MissionDTO}.
 */
@Mapper(componentModel = "spring", uses = {CityMapper.class})
public interface MissionMapper extends EntityMapper<MissionDTO, Mission> {

    @Mapping(source = "city.id", target = "cityId")
    MissionDTO toDto(Mission mission);

    @Mapping(target = "images", ignore = true)
    @Mapping(target = "removeImage", ignore = true)
    @Mapping(source = "cityId", target = "city")
    Mission toEntity(MissionDTO missionDTO);

    default Mission fromId(Long id) {
        if (id == null) {
            return null;
        }
        Mission mission = new Mission();
        mission.setId(id);
        return mission;
    }
}
