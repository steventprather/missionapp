package com.prather.missionapp.service;

import com.prather.missionapp.domain.CityImage;
import com.prather.missionapp.domain.dto.CityImageDTO;
import com.prather.missionapp.repository.CityImageRepository;
import com.prather.missionapp.service.mapper.CityImageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link CityImage}.
 */
@Service
@Transactional
public class CityImageService {

    private final Logger log = LoggerFactory.getLogger(CityImageService.class);

    private final CityImageRepository cityImageRepository;

    private final CityImageMapper cityImageMapper;

    public CityImageService(CityImageRepository cityImageRepository, CityImageMapper cityImageMapper) {
        this.cityImageRepository = cityImageRepository;
        this.cityImageMapper = cityImageMapper;
    }

    /**
     * Save a cityImage.
     *
     * @param cityImageDTO the entity to save.
     * @return the persisted entity.
     */
    public CityImageDTO save(CityImageDTO cityImageDTO) {
        log.debug("Request to save CityImage : {}", cityImageDTO);
        CityImage cityImage = cityImageMapper.toEntity(cityImageDTO);
        cityImage = cityImageRepository.save(cityImage);
        return cityImageMapper.toDto(cityImage);
    }

    /**
     * Get all the cityImages.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CityImageDTO> findAll() {
        log.debug("Request to get all CityImages");
        return cityImageRepository.findAll().stream()
            .map(cityImageMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one cityImage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CityImageDTO> findOne(Long id) {
        log.debug("Request to get CityImage : {}", id);
        return cityImageRepository.findById(id)
            .map(cityImageMapper::toDto);
    }

    /**
     * Delete the cityImage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CityImage : {}", id);

        cityImageRepository.deleteById(id);
    }
}
