package com.prather.missionapp.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prather.missionapp.domain.Objective;
import com.prather.missionapp.domain.dto.ObjectiveDTO;
import com.prather.missionapp.repository.ObjectiveRepository;
import com.prather.missionapp.service.mapper.ObjectiveMapper;

/**
 * Service Implementation for managing {@link Objective}.
 */
@Service
@Transactional
public class ObjectiveService {

    private final Logger log = LoggerFactory.getLogger(ObjectiveService.class);

    private final ObjectiveRepository objectiveRepository;

    private final ObjectiveMapper objectiveMapper;

    public ObjectiveService(ObjectiveRepository objectiveRepository, ObjectiveMapper objectiveMapper) {
        this.objectiveRepository = objectiveRepository;
        this.objectiveMapper = objectiveMapper;
    }

    /**
     * Save a objective.
     *
     * @param objectiveDTO the entity to save.
     * @return the persisted entity.
     */
    public ObjectiveDTO save(ObjectiveDTO objectiveDTO) {
        log.debug("Request to save Objective : {}", objectiveDTO);
        Objective objective = objectiveMapper.toEntity(objectiveDTO);
        objective = objectiveRepository.save(objective);
        return objectiveMapper.toDto(objective);
    }

    /**
     * Get all the objectives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ObjectiveDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Objectives");
        return objectiveRepository.findAll(pageable)
            .map(objectiveMapper::toDto);
    }


    /**
     * Get one objective by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ObjectiveDTO> findOne(Long id) {
        log.debug("Request to get Objective : {}", id);
        return objectiveRepository.findById(id)
            .map(objectiveMapper::toDto);
    }

    /**
     * Delete the objective by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Objective : {}", id);

        objectiveRepository.deleteById(id);
    }
    
    /**
     * Get all the objectives for a mission
     *
     * @param missionId
     * @param pageable the pagination information. 
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ObjectiveDTO> findByMission(Long missionId, Pageable pageable) {
    	
    	 Specification<Objective> spec = (objective, cq, cb) -> cb.equal(objective.get("mission").get("id"), missionId);
		return objectiveRepository.findAll(spec, pageable)
			.map(objectiveMapper::toDto);
	}
    
}
