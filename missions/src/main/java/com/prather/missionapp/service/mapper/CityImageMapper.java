package com.prather.missionapp.service.mapper;


import com.prather.missionapp.domain.*;
import com.prather.missionapp.domain.dto.CityImageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CityImage} and its DTO {@link CityImageDTO}.
 */
@Mapper(componentModel = "spring", uses = {CityMapper.class})
public interface CityImageMapper extends EntityMapper<CityImageDTO, CityImage> {

    @Mapping(source = "city.id", target = "cityId")
    CityImageDTO toDto(CityImage cityImage);

    @Mapping(source = "cityId", target = "city")
    CityImage toEntity(CityImageDTO cityImageDTO);

    default CityImage fromId(Long id) {
        if (id == null) {
            return null;
        }
        CityImage cityImage = new CityImage();
        cityImage.setId(id);
        return cityImage;
    }
}
