package com.prather.missionapp.service.mapper;


import com.prather.missionapp.domain.*;
import com.prather.missionapp.domain.dto.ObjectiveImageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ObjectiveImage} and its DTO {@link ObjectiveImageDTO}.
 */
@Mapper(componentModel = "spring", uses = {ObjectiveMapper.class})
public interface ObjectiveImageMapper extends EntityMapper<ObjectiveImageDTO, ObjectiveImage> {

    @Mapping(source = "objective.id", target = "objectiveId")
    ObjectiveImageDTO toDto(ObjectiveImage objectiveImage);

    @Mapping(source = "objectiveId", target = "objective")
    ObjectiveImage toEntity(ObjectiveImageDTO objectiveImageDTO);

    default ObjectiveImage fromId(Long id) {
        if (id == null) {
            return null;
        }
        ObjectiveImage objectiveImage = new ObjectiveImage();
        objectiveImage.setId(id);
        return objectiveImage;
    }
}
