package com.prather.missionapp.service;

import com.prather.missionapp.domain.MissionImage;
import com.prather.missionapp.domain.dto.MissionImageDTO;
import com.prather.missionapp.repository.MissionImageRepository;
import com.prather.missionapp.service.mapper.MissionImageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link MissionImage}.
 */
@Service
@Transactional
public class MissionImageService {

    private final Logger log = LoggerFactory.getLogger(MissionImageService.class);

    private final MissionImageRepository missionImageRepository;

    private final MissionImageMapper missionImageMapper;

    public MissionImageService(MissionImageRepository missionImageRepository, MissionImageMapper missionImageMapper) {
        this.missionImageRepository = missionImageRepository;
        this.missionImageMapper = missionImageMapper;
    }

    /**
     * Save a missionImage.
     *
     * @param missionImageDTO the entity to save.
     * @return the persisted entity.
     */
    public MissionImageDTO save(MissionImageDTO missionImageDTO) {
        log.debug("Request to save MissionImage : {}", missionImageDTO);
        MissionImage missionImage = missionImageMapper.toEntity(missionImageDTO);
        missionImage = missionImageRepository.save(missionImage);
        return missionImageMapper.toDto(missionImage);
    }

    /**
     * Get all the missionImages.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<MissionImageDTO> findAll() {
        log.debug("Request to get all MissionImages");
        return missionImageRepository.findAll().stream()
            .map(missionImageMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one missionImage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MissionImageDTO> findOne(Long id) {
        log.debug("Request to get MissionImage : {}", id);
        return missionImageRepository.findById(id)
            .map(missionImageMapper::toDto);
    }

    /**
     * Delete the missionImage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MissionImage : {}", id);

        missionImageRepository.deleteById(id);
    }
}
