package com.prather.missionapp.service.mapper;


import com.prather.missionapp.domain.*;
import com.prather.missionapp.domain.dto.ObjectiveDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Objective} and its DTO {@link ObjectiveDTO}.
 */
@Mapper(componentModel = "spring", uses = {MissionMapper.class})
public interface ObjectiveMapper extends EntityMapper<ObjectiveDTO, Objective> {

    @Mapping(source = "mission.id", target = "missionId")
    @Mapping(source = "parent.id", target = "parentId")
    ObjectiveDTO toDto(Objective objective);

    @Mapping(target = "images", ignore = true)
    @Mapping(target = "removeImage", ignore = true)
    @Mapping(source = "missionId", target = "mission")
    @Mapping(source = "parentId", target = "parent")
    Objective toEntity(ObjectiveDTO objectiveDTO);

    default Objective fromId(Long id) {
        if (id == null) {
            return null;
        }
        Objective objective = new Objective();
        objective.setId(id);
        return objective;
    }
}
