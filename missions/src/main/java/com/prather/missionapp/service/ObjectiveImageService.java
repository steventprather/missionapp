package com.prather.missionapp.service;

import com.prather.missionapp.domain.ObjectiveImage;
import com.prather.missionapp.domain.dto.ObjectiveImageDTO;
import com.prather.missionapp.repository.ObjectiveImageRepository;
import com.prather.missionapp.service.mapper.ObjectiveImageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ObjectiveImage}.
 */
@Service
@Transactional
public class ObjectiveImageService {

    private final Logger log = LoggerFactory.getLogger(ObjectiveImageService.class);

    private final ObjectiveImageRepository objectiveImageRepository;

    private final ObjectiveImageMapper objectiveImageMapper;

    public ObjectiveImageService(ObjectiveImageRepository objectiveImageRepository, ObjectiveImageMapper objectiveImageMapper) {
        this.objectiveImageRepository = objectiveImageRepository;
        this.objectiveImageMapper = objectiveImageMapper;
    }

    /**
     * Save a objectiveImage.
     *
     * @param objectiveImageDTO the entity to save.
     * @return the persisted entity.
     */
    public ObjectiveImageDTO save(ObjectiveImageDTO objectiveImageDTO) {
        log.debug("Request to save ObjectiveImage : {}", objectiveImageDTO);
        ObjectiveImage objectiveImage = objectiveImageMapper.toEntity(objectiveImageDTO);
        objectiveImage = objectiveImageRepository.save(objectiveImage);
        return objectiveImageMapper.toDto(objectiveImage);
    }

    /**
     * Get all the objectiveImages.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ObjectiveImageDTO> findAll() {
        log.debug("Request to get all ObjectiveImages");
        return objectiveImageRepository.findAll().stream()
            .map(objectiveImageMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one objectiveImage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ObjectiveImageDTO> findOne(Long id) {
        log.debug("Request to get ObjectiveImage : {}", id);
        return objectiveImageRepository.findById(id)
            .map(objectiveImageMapper::toDto);
    }

    /**
     * Delete the objectiveImage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ObjectiveImage : {}", id);

        objectiveImageRepository.deleteById(id);
    }
}
