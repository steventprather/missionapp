package com.prather.missionapp.service.mapper;


import com.prather.missionapp.domain.*;
import com.prather.missionapp.domain.dto.MissionImageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MissionImage} and its DTO {@link MissionImageDTO}.
 */
@Mapper(componentModel = "spring", uses = {MissionMapper.class})
public interface MissionImageMapper extends EntityMapper<MissionImageDTO, MissionImage> {

    @Mapping(source = "mission.id", target = "missionId")
    MissionImageDTO toDto(MissionImage missionImage);

    @Mapping(source = "missionId", target = "mission")
    MissionImage toEntity(MissionImageDTO missionImageDTO);

    default MissionImage fromId(Long id) {
        if (id == null) {
            return null;
        }
        MissionImage missionImage = new MissionImage();
        missionImage.setId(id);
        return missionImage;
    }
}
