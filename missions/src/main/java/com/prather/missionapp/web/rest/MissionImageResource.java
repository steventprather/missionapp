package com.prather.missionapp.web.rest;

import com.prather.missionapp.domain.dto.MissionImageDTO;
import com.prather.missionapp.service.MissionImageService;
import com.prather.missionapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.prather.missionapp.domain.MissionImage}.
 */
@RestController
@RequestMapping("/api")
public class MissionImageResource {

    private final Logger log = LoggerFactory.getLogger(MissionImageResource.class);

    private static final String ENTITY_NAME = "missionsMissionImage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MissionImageService missionImageService;

    public MissionImageResource(MissionImageService missionImageService) {
        this.missionImageService = missionImageService;
    }

    /**
     * {@code POST  /mission-images} : Create a new missionImage.
     *
     * @param missionImageDTO the missionImageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new missionImageDTO, or with status {@code 400 (Bad Request)} if the missionImage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mission-images")
    public ResponseEntity<MissionImageDTO> createMissionImage(@RequestBody MissionImageDTO missionImageDTO) throws URISyntaxException {
        log.debug("REST request to save MissionImage : {}", missionImageDTO);
        if (missionImageDTO.getId() != null) {
            throw new BadRequestAlertException("A new missionImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MissionImageDTO result = missionImageService.save(missionImageDTO);
        return ResponseEntity.created(new URI("/api/mission-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mission-images} : Updates an existing missionImage.
     *
     * @param missionImageDTO the missionImageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated missionImageDTO,
     * or with status {@code 400 (Bad Request)} if the missionImageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the missionImageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mission-images")
    public ResponseEntity<MissionImageDTO> updateMissionImage(@RequestBody MissionImageDTO missionImageDTO) throws URISyntaxException {
        log.debug("REST request to update MissionImage : {}", missionImageDTO);
        if (missionImageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MissionImageDTO result = missionImageService.save(missionImageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, missionImageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mission-images} : get all the missionImages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of missionImages in body.
     */
    @GetMapping("/mission-images")
    public List<MissionImageDTO> getAllMissionImages() {
        log.debug("REST request to get all MissionImages");
        return missionImageService.findAll();
    }

    /**
     * {@code GET  /mission-images/:id} : get the "id" missionImage.
     *
     * @param id the id of the missionImageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the missionImageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mission-images/{id}")
    public ResponseEntity<MissionImageDTO> getMissionImage(@PathVariable Long id) {
        log.debug("REST request to get MissionImage : {}", id);
        Optional<MissionImageDTO> missionImageDTO = missionImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(missionImageDTO);
    }

    /**
     * {@code DELETE  /mission-images/:id} : delete the "id" missionImage.
     *
     * @param id the id of the missionImageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mission-images/{id}")
    public ResponseEntity<Void> deleteMissionImage(@PathVariable Long id) {
        log.debug("REST request to delete MissionImage : {}", id);

        missionImageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
