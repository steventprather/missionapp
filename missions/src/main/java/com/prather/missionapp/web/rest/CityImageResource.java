package com.prather.missionapp.web.rest;

import com.prather.missionapp.domain.dto.CityImageDTO;
import com.prather.missionapp.service.CityImageService;
import com.prather.missionapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.prather.missionapp.domain.CityImage}.
 */
@RestController
@RequestMapping("/api")
public class CityImageResource {

    private final Logger log = LoggerFactory.getLogger(CityImageResource.class);

    private static final String ENTITY_NAME = "missionsCityImage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CityImageService cityImageService;

    public CityImageResource(CityImageService cityImageService) {
        this.cityImageService = cityImageService;
    }

    /**
     * {@code POST  /city-images} : Create a new cityImage.
     *
     * @param cityImageDTO the cityImageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cityImageDTO, or with status {@code 400 (Bad Request)} if the cityImage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/city-images")
    public ResponseEntity<CityImageDTO> createCityImage(@RequestBody CityImageDTO cityImageDTO) throws URISyntaxException {
        log.debug("REST request to save CityImage : {}", cityImageDTO);
        if (cityImageDTO.getId() != null) {
            throw new BadRequestAlertException("A new cityImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CityImageDTO result = cityImageService.save(cityImageDTO);
        return ResponseEntity.created(new URI("/api/city-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /city-images} : Updates an existing cityImage.
     *
     * @param cityImageDTO the cityImageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cityImageDTO,
     * or with status {@code 400 (Bad Request)} if the cityImageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cityImageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/city-images")
    public ResponseEntity<CityImageDTO> updateCityImage(@RequestBody CityImageDTO cityImageDTO) throws URISyntaxException {
        log.debug("REST request to update CityImage : {}", cityImageDTO);
        if (cityImageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CityImageDTO result = cityImageService.save(cityImageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cityImageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /city-images} : get all the cityImages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cityImages in body.
     */
    @GetMapping("/city-images")
    public List<CityImageDTO> getAllCityImages() {
        log.debug("REST request to get all CityImages");
        return cityImageService.findAll();
    }

    /**
     * {@code GET  /city-images/:id} : get the "id" cityImage.
     *
     * @param id the id of the cityImageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cityImageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/city-images/{id}")
    public ResponseEntity<CityImageDTO> getCityImage(@PathVariable Long id) {
        log.debug("REST request to get CityImage : {}", id);
        Optional<CityImageDTO> cityImageDTO = cityImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cityImageDTO);
    }

    /**
     * {@code DELETE  /city-images/:id} : delete the "id" cityImage.
     *
     * @param id the id of the cityImageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/city-images/{id}")
    public ResponseEntity<Void> deleteCityImage(@PathVariable Long id) {
        log.debug("REST request to delete CityImage : {}", id);

        cityImageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
