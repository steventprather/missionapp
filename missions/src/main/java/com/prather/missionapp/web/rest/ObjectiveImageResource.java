package com.prather.missionapp.web.rest;

import com.prather.missionapp.domain.dto.ObjectiveImageDTO;
import com.prather.missionapp.service.ObjectiveImageService;
import com.prather.missionapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.prather.missionapp.domain.ObjectiveImage}.
 */
@RestController
@RequestMapping("/api")
public class ObjectiveImageResource {

    private final Logger log = LoggerFactory.getLogger(ObjectiveImageResource.class);

    private static final String ENTITY_NAME = "missionsObjectiveImage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ObjectiveImageService objectiveImageService;

    public ObjectiveImageResource(ObjectiveImageService objectiveImageService) {
        this.objectiveImageService = objectiveImageService;
    }

    /**
     * {@code POST  /objective-images} : Create a new objectiveImage.
     *
     * @param objectiveImageDTO the objectiveImageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new objectiveImageDTO, or with status {@code 400 (Bad Request)} if the objectiveImage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/objective-images")
    public ResponseEntity<ObjectiveImageDTO> createObjectiveImage(@RequestBody ObjectiveImageDTO objectiveImageDTO) throws URISyntaxException {
        log.debug("REST request to save ObjectiveImage : {}", objectiveImageDTO);
        if (objectiveImageDTO.getId() != null) {
            throw new BadRequestAlertException("A new objectiveImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ObjectiveImageDTO result = objectiveImageService.save(objectiveImageDTO);
        return ResponseEntity.created(new URI("/api/objective-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /objective-images} : Updates an existing objectiveImage.
     *
     * @param objectiveImageDTO the objectiveImageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated objectiveImageDTO,
     * or with status {@code 400 (Bad Request)} if the objectiveImageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the objectiveImageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/objective-images")
    public ResponseEntity<ObjectiveImageDTO> updateObjectiveImage(@RequestBody ObjectiveImageDTO objectiveImageDTO) throws URISyntaxException {
        log.debug("REST request to update ObjectiveImage : {}", objectiveImageDTO);
        if (objectiveImageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ObjectiveImageDTO result = objectiveImageService.save(objectiveImageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, objectiveImageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /objective-images} : get all the objectiveImages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of objectiveImages in body.
     */
    @GetMapping("/objective-images")
    public List<ObjectiveImageDTO> getAllObjectiveImages() {
        log.debug("REST request to get all ObjectiveImages");
        return objectiveImageService.findAll();
    }

    /**
     * {@code GET  /objective-images/:id} : get the "id" objectiveImage.
     *
     * @param id the id of the objectiveImageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the objectiveImageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/objective-images/{id}")
    public ResponseEntity<ObjectiveImageDTO> getObjectiveImage(@PathVariable Long id) {
        log.debug("REST request to get ObjectiveImage : {}", id);
        Optional<ObjectiveImageDTO> objectiveImageDTO = objectiveImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(objectiveImageDTO);
    }

    /**
     * {@code DELETE  /objective-images/:id} : delete the "id" objectiveImage.
     *
     * @param id the id of the objectiveImageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/objective-images/{id}")
    public ResponseEntity<Void> deleteObjectiveImage(@PathVariable Long id) {
        log.debug("REST request to delete ObjectiveImage : {}", id);

        objectiveImageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
