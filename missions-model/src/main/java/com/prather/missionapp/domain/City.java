package com.prather.missionapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A City.
 */
@Entity
@Table(name = "city")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class City implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "text")
    private String text;

    @Column(name = "image_base")
    private String imageBase;

    @Column(name = "point_goal")
    private Integer pointGoal;

    @OneToMany(mappedBy = "city")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<CityImage> images = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public City name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public City text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageBase() {
        return imageBase;
    }

    public City imageBase(String imageBase) {
        this.imageBase = imageBase;
        return this;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public Integer getPointGoal() {
        return pointGoal;
    }

    public City pointGoal(Integer pointGoal) {
        this.pointGoal = pointGoal;
        return this;
    }

    public void setPointGoal(Integer pointGoal) {
        this.pointGoal = pointGoal;
    }

    public Set<CityImage> getImages() {
        return images;
    }

    public City images(Set<CityImage> cityImages) {
        this.images = cityImages;
        return this;
    }

    public City addImage(CityImage cityImage) {
        this.images.add(cityImage);
        cityImage.setCity(this);
        return this;
    }

    public City removeImage(CityImage cityImage) {
        this.images.remove(cityImage);
        cityImage.setCity(null);
        return this;
    }

    public void setImages(Set<CityImage> cityImages) {
        this.images = cityImages;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof City)) {
            return false;
        }
        return id != null && id.equals(((City) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "City{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", text='" + getText() + "'" +
            ", imageBase='" + getImageBase() + "'" +
            ", pointGoal=" + getPointGoal() +
            "}";
    }
}
