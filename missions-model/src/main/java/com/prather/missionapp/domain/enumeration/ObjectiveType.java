package com.prather.missionapp.domain.enumeration;

/**
 * The ObjectiveType enumeration.
 */
public enum ObjectiveType {
    PARENT, PERITEM, CHECKBOX
}
