package com.prather.missionapp.domain.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.prather.missionapp.domain.CityImage} entity.
 */
public class CityImageDTO implements Serializable {
    
    private Long id;

    private String fileName;


    private Long cityId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CityImageDTO)) {
            return false;
        }

        return id != null && id.equals(((CityImageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CityImageDTO{" +
            "id=" + getId() +
            ", fileName='" + getFileName() + "'" +
            ", cityId=" + getCityId() +
            "}";
    }
}
