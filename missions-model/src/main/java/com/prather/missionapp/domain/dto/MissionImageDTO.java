package com.prather.missionapp.domain.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.prather.missionapp.domain.MissionImage} entity.
 */
public class MissionImageDTO implements Serializable {
    
    private Long id;

    private String fileName;


    private Long missionId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getMissionId() {
        return missionId;
    }

    public void setMissionId(Long missionId) {
        this.missionId = missionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MissionImageDTO)) {
            return false;
        }

        return id != null && id.equals(((MissionImageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MissionImageDTO{" +
            "id=" + getId() +
            ", fileName='" + getFileName() + "'" +
            ", missionId=" + getMissionId() +
            "}";
    }
}
