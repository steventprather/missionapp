package com.prather.missionapp.domain.dto;

import java.io.Serializable;
import com.prather.missionapp.domain.enumeration.ObjectiveType;

/**
 * A DTO for the {@link com.prather.missionapp.domain.Objective} entity.
 */
public class ObjectiveDTO implements Serializable {
    
    private Long id;

    private String name;

    private String text;

    private ObjectiveType type;

    private Integer order;

    private Integer maxPoints;

    private Integer pointsPerItem;

    private Integer points;

    private String imageBase;


    private Long missionId;

    private Long parentId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ObjectiveType getType() {
        return type;
    }

    public void setType(ObjectiveType type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Integer getPointsPerItem() {
        return pointsPerItem;
    }

    public void setPointsPerItem(Integer pointsPerItem) {
        this.pointsPerItem = pointsPerItem;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getImageBase() {
        return imageBase;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public Long getMissionId() {
        return missionId;
    }

    public void setMissionId(Long missionId) {
        this.missionId = missionId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long objectiveId) {
        this.parentId = objectiveId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ObjectiveDTO)) {
            return false;
        }

        return id != null && id.equals(((ObjectiveDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ObjectiveDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", text='" + getText() + "'" +
            ", type='" + getType() + "'" +
            ", order=" + getOrder() +
            ", maxPoints=" + getMaxPoints() +
            ", pointsPerItem=" + getPointsPerItem() +
            ", points=" + getPoints() +
            ", imageBase='" + getImageBase() + "'" +
            ", missionId=" + getMissionId() +
            ", parentId=" + getParentId() +
            "}";
    }
}
