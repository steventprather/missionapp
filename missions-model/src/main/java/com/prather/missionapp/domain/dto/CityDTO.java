package com.prather.missionapp.domain.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.prather.missionapp.domain.City} entity.
 */
public class CityDTO implements Serializable {
    
    private Long id;

    private String name;

    private String text;

    private String imageBase;

    private Integer pointGoal;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageBase() {
        return imageBase;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public Integer getPointGoal() {
        return pointGoal;
    }

    public void setPointGoal(Integer pointGoal) {
        this.pointGoal = pointGoal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CityDTO)) {
            return false;
        }

        return id != null && id.equals(((CityDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CityDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", text='" + getText() + "'" +
            ", imageBase='" + getImageBase() + "'" +
            ", pointGoal=" + getPointGoal() +
            "}";
    }
}
