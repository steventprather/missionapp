package com.prather.missionapp.domain.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.prather.missionapp.domain.ObjectiveImage} entity.
 */
public class ObjectiveImageDTO implements Serializable {
    
    private Long id;

    private String fileName;


    private Long objectiveId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getObjectiveId() {
        return objectiveId;
    }

    public void setObjectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ObjectiveImageDTO)) {
            return false;
        }

        return id != null && id.equals(((ObjectiveImageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ObjectiveImageDTO{" +
            "id=" + getId() +
            ", fileName='" + getFileName() + "'" +
            ", objectiveId=" + getObjectiveId() +
            "}";
    }
}
