package com.prather.missionapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Mission.
 */
@Entity
@Table(name = "mission")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Mission implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "text")
    private String text;

    @Column(name = "image_base")
    private String imageBase;

    @OneToMany(mappedBy = "mission")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<MissionImage> images = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "missions", allowSetters = true)
    private City city;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Mission name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public Mission text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageBase() {
        return imageBase;
    }

    public Mission imageBase(String imageBase) {
        this.imageBase = imageBase;
        return this;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public Set<MissionImage> getImages() {
        return images;
    }

    public Mission images(Set<MissionImage> missionImages) {
        this.images = missionImages;
        return this;
    }

    public Mission addImage(MissionImage missionImage) {
        this.images.add(missionImage);
        missionImage.setMission(this);
        return this;
    }

    public Mission removeImage(MissionImage missionImage) {
        this.images.remove(missionImage);
        missionImage.setMission(null);
        return this;
    }

    public void setImages(Set<MissionImage> missionImages) {
        this.images = missionImages;
    }

    public City getCity() {
        return city;
    }

    public Mission city(City city) {
        this.city = city;
        return this;
    }

    public void setCity(City city) {
        this.city = city;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Mission)) {
            return false;
        }
        return id != null && id.equals(((Mission) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Mission{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", text='" + getText() + "'" +
            ", imageBase='" + getImageBase() + "'" +
            "}";
    }
}
