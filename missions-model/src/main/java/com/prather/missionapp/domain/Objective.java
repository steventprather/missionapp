package com.prather.missionapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.prather.missionapp.domain.enumeration.ObjectiveType;

/**
 * A Objective.
 */
@Entity
@Table(name = "objective")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Objective implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "text")
    private String text;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private ObjectiveType type;

    @Column(name = "jhi_order")
    private Integer order;

    @Column(name = "max_points")
    private Integer maxPoints;

    @Column(name = "points_per_item")
    private Integer pointsPerItem;

    @Column(name = "points")
    private Integer points;

    @Column(name = "image_base")
    private String imageBase;

    @OneToMany(mappedBy = "objective")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ObjectiveImage> images = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "objectives", allowSetters = true)
    private Mission mission;

    @ManyToOne
    @JsonIgnoreProperties(value = "objectives", allowSetters = true)
    private Objective parent;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Objective name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public Objective text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ObjectiveType getType() {
        return type;
    }

    public Objective type(ObjectiveType type) {
        this.type = type;
        return this;
    }

    public void setType(ObjectiveType type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public Objective order(Integer order) {
        this.order = order;
        return this;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public Objective maxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
        return this;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Integer getPointsPerItem() {
        return pointsPerItem;
    }

    public Objective pointsPerItem(Integer pointsPerItem) {
        this.pointsPerItem = pointsPerItem;
        return this;
    }

    public void setPointsPerItem(Integer pointsPerItem) {
        this.pointsPerItem = pointsPerItem;
    }

    public Integer getPoints() {
        return points;
    }

    public Objective points(Integer points) {
        this.points = points;
        return this;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getImageBase() {
        return imageBase;
    }

    public Objective imageBase(String imageBase) {
        this.imageBase = imageBase;
        return this;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public Set<ObjectiveImage> getImages() {
        return images;
    }

    public Objective images(Set<ObjectiveImage> objectiveImages) {
        this.images = objectiveImages;
        return this;
    }

    public Objective addImage(ObjectiveImage objectiveImage) {
        this.images.add(objectiveImage);
        objectiveImage.setObjective(this);
        return this;
    }

    public Objective removeImage(ObjectiveImage objectiveImage) {
        this.images.remove(objectiveImage);
        objectiveImage.setObjective(null);
        return this;
    }

    public void setImages(Set<ObjectiveImage> objectiveImages) {
        this.images = objectiveImages;
    }

    public Mission getMission() {
        return mission;
    }

    public Objective mission(Mission mission) {
        this.mission = mission;
        return this;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public Objective getParent() {
        return parent;
    }

    public Objective parent(Objective objective) {
        this.parent = objective;
        return this;
    }

    public void setParent(Objective objective) {
        this.parent = objective;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Objective)) {
            return false;
        }
        return id != null && id.equals(((Objective) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Objective{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", text='" + getText() + "'" +
            ", type='" + getType() + "'" +
            ", order=" + getOrder() +
            ", maxPoints=" + getMaxPoints() +
            ", pointsPerItem=" + getPointsPerItem() +
            ", points=" + getPoints() +
            ", imageBase='" + getImageBase() + "'" +
            "}";
    }
}
