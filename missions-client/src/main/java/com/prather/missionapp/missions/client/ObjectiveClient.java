package com.prather.missionapp.missions.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import com.prather.missionapp.domain.dto.ObjectiveDTO;

@FeignClient(name = "missions/api/objectives")
public interface ObjectiveClient {
    
    @GetMapping(value = "/byMission/{missionId}")
    List < ObjectiveDTO > getByMission(@PathVariable("missionId") Long missionId);
    
    @GetMapping(value = "/{id}")
    ObjectiveDTO getObjective(@PathVariable("id") Long id);

}