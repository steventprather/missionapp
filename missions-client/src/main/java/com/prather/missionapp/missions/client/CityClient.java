package com.prather.missionapp.missions.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prather.missionapp.domain.dto.CityDTO;

@FeignClient(name = "missions/api/cities")
public interface CityClient {
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    List < CityDTO > getAll();

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    CityDTO getCity(@PathVariable("id") Long id);
    
}