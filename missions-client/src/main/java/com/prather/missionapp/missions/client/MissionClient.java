package com.prather.missionapp.missions.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import com.prather.missionapp.domain.dto.MissionDTO;

@FeignClient(name = "missions/api/missions")
public interface MissionClient {
    
    @GetMapping(value = "/byCity/{cityId}")
    List < MissionDTO > getByCity(@PathVariable("cityId") Long cityId);
    
    @GetMapping(value = "/{id}")
    MissionDTO getMission(@PathVariable("id") Long id);

}